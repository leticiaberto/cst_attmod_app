% fid = fopen(INPUT_FILE,'rt');
% if (fid == -1) 
%     disp('File not found');
% end
% num_linhas = 0;
% semaforo = 0;
% linha = 0;
% while (semaforo==0),
%     fid
%    tline = fgets(fid);                      %Faz leitura de uma linha inteira (qualquer n?mero de dimens?es) para tline
%    if (tline == -1)                         %Verifica se EOF
%        semaforo = 1;
%    else                                     %Linha v?lida de dados 
%       linha = linha+1;
%       if (linha == 1)                       %A primeira linha armazena os nomes dos sensores
%           nomes = strread(tline, '%s');
%           [num_colunas lixo] = size(nomes);
%           dados = zeros(num_linhas,num_colunas);
%           
function [r]=exponentialGrowDecayBottomUp(pre, ts, tm, t)
    h=0;

    if ((t-pre) > 0) h=1;
    elseif ((t-pre) == 0)  h=0.5;
	else h=0;
    end
	
	
    r = ((exp(-1*(t-pre)/ tm) - exp(-1*(t-pre)/ ts)) * h);
    

