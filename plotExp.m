BOTTOM_UP_PRE_TIME = 2000;
BOTTOM_UP_EXCITATORY_TIME = 2000;
BOTTOM_UP_INHIBITORY_TIME = 4000;
TS = 100;
TM = 1000;



t0=0;
newvalue = zeros(8000);

for i=1:8000
    if((t0+BOTTOM_UP_PRE_TIME+BOTTOM_UP_EXCITATORY_TIME >= i) && (t0+BOTTOM_UP_PRE_TIME <= i))
        t = i - t0;
        newvalue(i)= exponentialGrowDecayBottomUp(BOTTOM_UP_PRE_TIME, TS, TM, t);
    elseif (((t0+BOTTOM_UP_PRE_TIME+BOTTOM_UP_EXCITATORY_TIME+BOTTOM_UP_INHIBITORY_TIME) >= i) && ((t0+BOTTOM_UP_PRE_TIME+BOTTOM_UP_EXCITATORY_TIME) <= i))
        t = i - t0;
        newvalue(i)= -exponentialGrowDecayBottomUp(BOTTOM_UP_PRE_TIME+BOTTOM_UP_EXCITATORY_TIME, TS, TM, t);
    else
        newvalue(i)=0;
    end
end
plot(newvalue);
    