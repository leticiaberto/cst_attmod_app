import numpy as np
import matplotlib.pyplot as plt
#Plota trajetoria do robot usando algoritmo de braitenberg
i = 0

#amarelo, verde, vermelho, rosa
obstaculosX =[+1.7750e+0, -1.2000e+0, -3.7500e-1, -1.0500e+0]
obstaculosY =[-1.7500e+0, -1.6500e+0, -9.5000e-1, +7.5000e-2]

#Robo atencional
attentiveRobot = [+5.0380e-2, +8.9452e-1]

with open('position2.txt', 'r') as f:
	x = f.read()
	#print(x)
	lines = x.split()
tam = len(lines)
posTup = []

#print(tam)
while i < (tam-1):
	x = float(lines[i])
	y = float(lines[i+1])
	pos = [x, y]#[-y, x]
	posTup.append(pos)
	#x_arr.append(float(lines[i]))
	#y_arr.append(float(lines[i+1]))
	#print(lines[i] , " ", lines[i+1])
	i = i+2

posX, posY = zip(*posTup)

x_ini = posX[0]
y_ini = posY[0]

x_fim = posX[len(posX)-1]
y_fim = posY[len(posY)-1]

# Plot trajectory
fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(obstaculosX,obstaculosY, c='m',marker='x', label = 'Objects')
ax.scatter(attentiveRobot[0],attentiveRobot[1], marker = 'v', c='r', s=100, zorder=3,label = 'Attentive robot')
ax.scatter(posX, posY, c='deeppink', label = 'Braitenberg robot')
ax.scatter(x_ini,y_ini,c='k',marker='^', s=100, zorder=3, label = 'Start Pose')#marker='^'
ax.scatter(x_fim,y_fim,c='gold',marker='D', s=100,  zorder=3, label = 'End')#-y_fim, x_Fim
ax.grid(True)
ax.legend(loc='best')
plt.title("Trajectory - Braitenberg Robot")
plt.xlabel("x")
plt.ylabel("y")
plt.savefig('trajectory'+'.pdf')
plt.show()
