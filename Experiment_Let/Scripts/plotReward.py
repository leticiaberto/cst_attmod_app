import numpy as np
import matplotlib.pyplot as plt

i = 0
#Plota os graficos individuas de cada reward

#Numero do experimento, numero de ações, reward do episodio, reward global

with open('rewards.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)

acaoEpisodio = []
episodioReward = []
globlReward = []

while i < (tam-1):
	ep = float(lines[i])
	acao = float(lines[i+1])
	epRew = float(lines[i+2])
	epG = float(lines[i+3])
	pos = [ep, epRew]
	posGlob = [ep, epG]
	acaoEp = [ep, acao]
	episodioReward.append(pos)
	globlReward.append(posGlob)
	acaoEpisodio.append(acaoEp)
	i = i+4

posX, posY = zip(*episodioReward)
posXG, posYG = zip(*globlReward)
posXA, posYA = zip(*acaoEpisodio)

# Plot episode reward
#fig, ax = plt.subplots(figsize=(8, 8))
#ax.scatter(posX, posY, label = 'reward')
#ax.grid(True)
#ax.legend(loc='best')
plt.plot(posX, posY)
plt.xlabel("Episode")
plt.ylabel("Reward")
plt.title('Episode Reward')
plt.savefig('EpisodeReward'+'.png')
plt.show()

# Plot global reward
#fig, ax = plt.subplots(figsize=(8, 8))
#ax.scatter(posXG, posYG, label = 'rewardGlobal')
#ax.grid(True)
#ax.legend(loc='best')
plt.plot(posXG, posYG)
plt.xlabel("Episode")
plt.ylabel("Reward")
plt.title('Global Reward')
plt.savefig('GlobalReward'+'.png')
plt.show()

# Plot episode actions
#fig, ax = plt.subplots(figsize=(8, 8))
#ax.scatter(posXA, posYA, label = 'episodeActions')
#ax.grid(True)
#ax.legend(loc='best')
plt.plot(posXA, posYA)
plt.xlabel("Episode")
plt.ylabel("Number of Actions")
plt.title('Actions per episode')
plt.savefig('Actions'+'.png')
plt.show()
