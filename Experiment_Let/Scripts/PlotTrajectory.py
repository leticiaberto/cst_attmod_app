import numpy as np
import matplotlib.pyplot as plt

i = 0

#verde, vermelho, rosa, azul, amarelo, preto, laranja, roxo
obstaculosX =[-9.5000e-1, -1.7500e+0, -1.0750e+0, -1.3750e+0, +5.0000e-2, +1.7250e+0, +1.3000e+0, +1.5250e+0]
obstaculosY =[1.2750e+0, +7.0000e-1, +7.5000e-2, -9.2500e-1, -1.0250e+0, +1.0000e+0, +2.5000e-2, -1.3000e+0]

with open('positionsGroundTruth.txt', 'r') as f:
	x = f.read()
	#print(x)
	lines = x.split()
tam = len(lines)
posTup = []

#print(tam)
while i < (tam-1):
	x = float(lines[i])
	y = float(lines[i+1])
	pos = [x, y]#[-y, x]
	posTup.append(pos)
	#x_arr.append(float(lines[i]))
	#y_arr.append(float(lines[i+1]))
	#print(lines[i] , " ", lines[i+1])
	i = i+2

posX, posY = zip(*posTup)

x_ini = posX[0]
y_ini = posY[0]

x_fim = posX[len(posX)-1]
y_fim = posY[len(posY)-1]

with open('exploringActionsNumber.txt', 'r') as f:
	actions = f.read()
actions = actions.replace("\n", "")

# Plot trajectory
fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(obstaculosX,obstaculosY, c='m',marker='x', label = 'Objects')
ax.scatter(posX, posY, label = 'Trajectory')
ax.scatter(x_ini,y_ini,c='g',marker='D', s=100, zorder=3, label = 'Start')#marker='^'
ax.scatter(x_fim,y_fim,c='r',marker='D', s=100,  zorder=3, label = 'End')#-y_fim, x_Fim
ax.grid(True)
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.title('Trajectory - ' + actions + ' actions')
plt.savefig('trajectory'+'.png')
plt.show()
