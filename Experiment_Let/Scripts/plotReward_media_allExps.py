import numpy as np
import matplotlib.pyplot as plt
import math

i = 0
#rewards 01 = Salience Turn
#rewards 02 = Sonar Turn
#rewards 03 = Salience + Sonar Turn

#This script plots the average and standard deviation (filled) of the rewards. The window choose is 10

#Numero do experimento, numero de ações, reward do episodio, reward global

color = 0
colorGraphs = ['m','b','g', "y", "r", "k"]
exps = ['01_Old_1200', '02_1kRuim', "03_1kRuim", "04_1kRuim", "05_1200", "06_1800"]

arq = 1
fig, axs = plt.subplots(2, sharex=True)#,gridspec_kw={'hspace': 0}
fig.suptitle('Learning - Turn Towards to Winner')

while (arq < 7):
	i = 0
	with open('rewards0'+str(arq)+'.txt', 'r') as f:
		x = f.read()
		lines = x.split()
	tam = len(lines)

	mediaRep = 0 #media das rewards dos episodios
	mediaAep = 0 #media das acoes dos episodios
	win = 10 #janela de episodios calculada

	acaoEpisodio = []
	episodioReward = []

	dpAcao = []
	dpReward = []
	mediasR = []
	mediasA = []

	k = 1
	j = 0

	while i < (tam-1):
		mediaAep = mediaAep + float(lines[i+1])
		mediaRep = mediaRep + float(lines[i+2])
		j = j + 1
		if(j == win*k):
			medRT = [j, mediaRep/win]
			medAT = [j, mediaAep/win]
			mediasA.append(mediaAep/win)
			mediasR.append(mediaRep/win)
			episodioReward.append(medRT)
			acaoEpisodio.append(medAT)
			mediaAep = 0
			mediaRep = 0
			k = k + 1
		i = i+4

	posX, posY = zip(*episodioReward)
	posXA, posYA = zip(*acaoEpisodio)

	axs[0].plot(posX, posY, c=colorGraphs[color], label = exps[color] + ' Average')
	##axs[0].fill_between(posX, [elemA + elemB for elemA, elemB in zip(posY, posYDR)], [elemA - elemB for elemA, elemB in zip(posY, posYDR)], color=colorGraphs[color], alpha=0.2)

	#axs[0].plot(posXDR, posYDR, label = 'Standard Deviation')


	axs[1].plot(posXA, posYA, c=colorGraphs[color], label = exps[color] + ' Average')
	##axs[1].fill_between(posXA, [elemA + elemB for elemA, elemB in zip(posYA, posYDA)], [elemA - elemB for elemA, elemB in zip(posYA, posYDA)], color=colorGraphs[color], alpha=0.2)

	#axs[1].plot(posXDA, posYDA, label = 'Standard Deviation')

	arq = arq + 1
	color += 1
leg = axs[0].legend(loc="upper left", bbox_to_anchor=[0, 1],
				 ncol=2, shadow=True, fancybox=True)
leg = axs[1].legend(loc="upper left", bbox_to_anchor=[0, 1],
                 ncol=2, shadow=True, fancybox=True)
axs[0].set_ylabel('Reward')
axs[1].set_ylabel('Actions')
axs[1].set_xlabel('Episode')
plt.savefig('Learning.pdf')
plt.show()
