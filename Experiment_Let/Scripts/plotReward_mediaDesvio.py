import numpy as np
import matplotlib.pyplot as plt
import math

i = 0

#This script plots the average and standard deviation of the rewards. The window choose is 10

#Numero do experimento, numero de ações, reward do episodio, reward global

with open('rewards.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)

mediaRep = 0 #media das rewards dos episodios
mediaAep = 0 #media das acoes dos episodios
win = 10 #janela de episodios calculada

acaoEpisodio = []
episodioReward = []

dpAcao = []
dpReward = []
mediasR = []
mediasA = []

k = 1
j = 0

while i < (tam-1):
	mediaAep = mediaAep + float(lines[i+1])
	mediaRep = mediaRep + float(lines[i+2])
	j = j + 1
	if(j == win*k):
		medRT = [j, mediaRep/win]
		medAT = [j, mediaAep/win]
		mediasA.append(mediaAep/win)
		mediasR.append(mediaRep/win)
		episodioReward.append(medRT)
		acaoEpisodio.append(medAT)
		mediaAep = 0
		mediaRep = 0
		k = k + 1
	i = i+4

posX, posY = zip(*episodioReward)
posXA, posYA = zip(*acaoEpisodio)
#print(medias[0])

i = 0
dpAcao = []
dpReward = []
k = 1
sumDifAc = 0
sumDifRe = 0
l = 0
j = 0

while i < (tam-1):
	sumDifAc = sumDifAc + math.pow((float(lines[i+1]) - mediasA[l]), 2)
	sumDifRe = sumDifRe + math.pow((float(lines[i+2]) - mediasR[l]), 2)
	j = j + 1
	if(j == win*k):
		dpA = [j, math.sqrt(sumDifAc/win)]
		dpR = [j, math.sqrt(sumDifRe/win)]
		dpReward.append(dpR)
		dpAcao.append(dpA)
		sumDifAc = 0
		sumDifRe = 0
		k = k + 1
		l = l + 1
	i = i+4

posXDR, posYDR = zip(*dpReward)
posXDA, posYDA = zip(*dpAcao)

fig, axs = plt.subplots(2, sharex=True)#,gridspec_kw={'hspace': 0}
fig.suptitle('Learning')
axs[0].plot(posX, posY, label = 'Average')
axs[0].plot(posXDR, posYDR, label = 'Standard Deviation')
leg = axs[0].legend(loc="upper left", bbox_to_anchor=[0, 1],
                 ncol=2, shadow=True, fancybox=True)
axs[1].plot(posXA, posYA, label = 'Average')
axs[1].plot(posXDA, posYDA, label = 'Standard Deviation')
leg = axs[1].legend(loc="upper left", bbox_to_anchor=[0, 1],
                 ncol=2, shadow=True, fancybox=True)
axs[0].set_ylabel('Reward')
axs[1].set_ylabel('Actions')
axs[1].set_xlabel('Episode')
plt.savefig('Learning.png')
plt.show()
