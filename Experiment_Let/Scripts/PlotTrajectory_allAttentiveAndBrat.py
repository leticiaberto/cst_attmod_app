import numpy as np
import matplotlib.pyplot as plt

#verde, vermelho, rosa, azul, amarelo, preto, laranja, roxo
obstaculosX =[-9.5000e-1, -1.7500e+0, -1.0750e+0, -1.3750e+0, +5.0000e-2, +1.7250e+0, +1.3000e+0, +1.5250e+0]
obstaculosY =[1.2750e+0, +7.0000e-1, +7.5000e-2, -9.2500e-1, -1.0250e+0, +1.0000e+0, +2.5000e-2, -1.3000e+0]

colorGraphs = ['b', 'lightblue','darkorange','navajowhite','g', 'darkseagreen']
c = 0

# Plot trajectory
fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(obstaculosX,obstaculosY, c='m',marker='x', label = 'Objects')

posIniAll = []
posFimAll = []

posIniAllBrat = []
posFimAllBrat = []

j = 1

while (j < 4):
	with open('positionsGroundTruth0'+str(j)+'.txt', 'r') as f:
		x = f.read()
		lines = x.split()
	tam = len(lines)
	posTup = []

	i = 0

	while i < (tam-1):
		x = float(lines[i])
		y = float(lines[i+1])
		pos = [x, y]#[-y, x]
		posTup.append(pos)
		i = i+2

	posX, posY = zip(*posTup)

	x_ini = posX[0]
	y_ini = posY[0]

	x_fim = posX[len(posX)-1]
	y_fim = posY[len(posY)-1]

	posIt = [posX[0], posY[0]]
	posItF = [posX[len(posX)-1], posY[len(posY)-1]]

	posIniAll.append(posIt)
	posFimAll.append(posItF)

	with open('exploringActionsNumber0'+str(j)+'.txt', 'r') as f:
		actions = f.read()
	actions = actions.replace("\n", "")

	ax.scatter(posX, posY, c = colorGraphs[c], label = actions + ' actions')
	c += 1
#######################################################################################################
	with open('position0'+str(j)+'.txt', 'r') as f:
		x = f.read()
		lines = x.split()
	tam = len(lines)
	posTup = []

	i = 0

	while i < (tam-1):
		x = float(lines[i])
		y = float(lines[i+1])
		pos = [x, y]#[-y, x]
		posTup.append(pos)
		i = i+2

	posX, posY = zip(*posTup)

	posIt = [posX[0], posY[0]]
	posItF = [posX[len(posX)-1], posY[len(posY)-1]]

	posIniAllBrat.append(posIt)
	posFimAllBrat.append(posItF)

	ax.scatter(posX, posY, c = colorGraphs[c], label ='Braitenberg')
	c += 1
######################################################################################################

	j = j + 1

posXI, posYI = zip(*posIniAll)
posXF, posYF = zip(*posFimAll)

posXIBrat, posYIBrat = zip(*posIniAllBrat)
posXFBrat, posYFBrat = zip(*posFimAllBrat)

ax.scatter(posXI[2],posYI[2],c='k',marker='^', s=100, zorder=3, label = 'Start Pose')
ax.scatter(posXF,posYF,c='r',marker='D', s=100,  zorder=3, label = 'End')

ax.scatter(posXI[0],posYI[0],c='k',marker='>', s=100, zorder=3)
ax.scatter(posXI[1],posYI[1],c='k',marker='v', s=100, zorder=3)

#Brateiberg
ax.scatter(posXIBrat[0],posYIBrat[0],c='k',marker='<', s=100, zorder=3)
ax.scatter(posXIBrat[1],posYIBrat[1],c='k',marker='^', s=100, zorder=3)
ax.scatter(posXIBrat[2],posYIBrat[2],c='k',marker='<', s=100, zorder=3)
ax.scatter(posXFBrat,posYFBrat,c='r',marker='D', s=100,  zorder=3)

ax.grid(True)
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.title('Trajectory using Sonar Without Winner Index')
plt.savefig('trajectorySaliency'+'.png')
plt.show()
