import numpy as np
import matplotlib.pyplot as plt

i = 0
#plota a reward e ação um por um em um grafico so com eixo x compartilhado

#Numero do experimento, numero de ações, reward do episodio, reward global

with open('rewards.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)

acaoEpisodio = []
episodioReward = []

while i < (tam-1):
	ep = float(lines[i])
	acao = float(lines[i+1])
	epRew = float(lines[i+2])
	pos = [ep, epRew]
	acaoEp = [ep, acao]
	episodioReward.append(pos)
	acaoEpisodio.append(acaoEp)
	i = i+4

posX, posY = zip(*episodioReward)
posXA, posYA = zip(*acaoEpisodio)

fig, axs = plt.subplots(2, sharex=True, gridspec_kw={'hspace': 0})
fig.suptitle('Learning')
axs[0].plot(posX, posY)
axs[1].plot(posXA, posYA)
axs[0].set_ylabel('Reward')
axs[1].set_ylabel('Number of actions')
axs[1].set_xlabel('Episode')
plt.savefig('LearningAll.png')
plt.show()
