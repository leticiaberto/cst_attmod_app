import numpy as np
import matplotlib.pyplot as plt
#Plota trajetoria do robot usando algoritmo de braitenberg
i = 0

#amarelo, vermelho, verde, azul
obstaculosX =[+2.0001e-1, -4.2499e-1, -1.2000e+0, -1.2250e+0]
obstaculosY =[-1.5000e+0, -1.5000e+0, -8.0001e-1, +4.2499e-1]

#Robo atencional
attentiveRobot = [-2.0795e-1, +8.9769e-1]

with open('position2.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)
posTup = []

#print(tam)
while i < (tam-1):
	x = float(lines[i])
	y = float(lines[i+1])
	pos = [x, y]
	posTup.append(pos)

	i = i+2

posX, posY = zip(*posTup)

x_ini = posX[0]
y_ini = posY[0]

x_fim = posX[len(posX)-1]
y_fim = posY[len(posY)-1]

# Plot trajectory
fig, ax = plt.subplots(figsize=(6, 6))
ax.scatter(obstaculosX,obstaculosY, c='m',marker='x', label = 'Objects')
ax.scatter(attentiveRobot[0],attentiveRobot[1], marker = 'v', c='r', s=100, zorder=3,label = 'Attentive robot')
ax.scatter(posX, posY, c='deeppink', label = 'Braitenberg robot')
ax.scatter(x_ini,y_ini,c='k',marker='<', s=100, zorder=3, label = 'Start Pose')
ax.scatter(x_fim,y_fim,c='gold',marker='D', s=100,  zorder=3, label = 'End')
ax.grid(True)
ax.legend(loc='best')
plt.title("Trajectory - Braitenberg Robot")
plt.xlabel("x")
plt.ylabel("y")
plt.savefig('trajectory'+'.pdf')
plt.show()
