import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

def plot_figures(figures, nrows = 1, ncols=1):
	"""Plot a dictionary of figures.

	Parameters
	----------
	figures : <title, figure> dictionary
	ncols : number of columns of subplots wanted in the display
	nrows : number of rows of subplots wanted in the figure
	"""

	fig, axeslist = plt.subplots(ncols=ncols, nrows=nrows, figsize=(80, 80))
	for ind,title in enumerate(figures):
	    axeslist.ravel()[ind].imshow(Image.open(image_datas[ind]), cmap=plt.gray())
	    #axeslist.ravel()[ind].set_title(title)
	    axeslist.ravel()[ind].set_axis_off()
	plt.tight_layout() # optional
	#plt.savefig('teste'+'.png')
	plt.show()


image_datas = []

number_of_im = 9
kp = 9
j = 10
lim = 90

while j <= lim:
	image_datas.append("PID_" + str(j) + "graus.png")
	j = j + 10


figures = {'im'+str(i): np.random.randn(100, 100) for i in range(number_of_im)}
# plot of the images in a figure, with 2 rows and 3 columns
plot_figures(figures, 3, 3)
