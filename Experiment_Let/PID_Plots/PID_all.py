import numpy as np
import matplotlib.pyplot as plt

i = 0
j = 1

arrTams = []
plotXFinal = []
plotYFinal = []
posIn = []
lim = 8
while j <= lim:
	with open('PIDOrien_Kp' + str(lim) + '_Ki'+ str(j) + '.txt', 'r') as f:
		x = f.read()
		lines = x.split()
	tam = len(lines)
	timeValor = []
	arrTams.append(tam)
	i = 0
	while i < (tam-1):
		time = float(lines[i])
		orien = float(lines[i+1])
		pos = [time, orien]
		timeValor.append(pos)
		i = i+2
	posIn.append(timeValor)
	j = j + 1
'''
graus	Radianos
10		0.174533
20		0.349066
30		0.523599
40		0.698132
50		0.872665
60		1.0472
70		1.22173
80		1.39626
90		1.5708
'''
objetivo = []
targetAngle = 0.174533
i = 1
tam = max(arrTams)
while i <= tam/2:
	posObj = [i, targetAngle]
	objetivo.append(posObj)
	i = i + 1
posXO, posYO = zip(*objetivo)


fig, ax = plt.subplots(figsize=(16, 16))

plt.plot(posXO,posYO, c='m', label = 'Goal')

colors = ['dodgerblue','r', 'gold', 'darkgreen', 'gray', 'coral', 'darkblue', 'deeppink', 'yellowgreen', 'cyan', 'purple', 'olive']
j = 0
while j < lim:
	posX_kp, posY_kp = zip(*posIn[j])
	plt.plot( posX_kp, posY_kp, c=colors[j], label = 'Kp = ' + str(lim) + '; Ki = ' + str(j+1))
	j = j + 1

ax.legend(loc='best')
plt.xlabel("Time")
plt.ylabel("Orientation")
plt.title('PID Turn 10 graus')
plt.savefig('PID_10graus'+'.png')
plt.show()
