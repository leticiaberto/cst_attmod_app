import numpy as np
import matplotlib.pyplot as plt


####################################Define this variables to each experiment###############################################
#Choose the color to plot and startPose. Pose1: c = 0; Pose2: c = 1; Pose3: c = 2. The options are in 'colorGraphs'
c = 2
#Choose the kind of calculate the states. The options are in 'titleGraph'
t = 1
#Chose the classification of the experiment. The options are in 'exp'
e = 1
############################################################################################################################


#verde, vermelho, rosa, azul, amarelo, preto, laranja, roxo
obstaculosX =[-9.5000e-1, -1.7500e+0, -1.0750e+0, -1.3750e+0, +5.0000e-2, +1.7250e+0, +1.3000e+0, +1.5250e+0]
obstaculosY =[1.2750e+0, +7.0000e-1, +7.5000e-2, -9.2500e-1, -1.0250e+0, +1.0000e+0, +2.5000e-2, -1.3000e+0]

#colorGraphs = ['b', 'lightblue','darkorange','navajowhite','g', 'darkseagreen']
colorGraphs = ['b', 'darkorange','g', 'darkgray']
titleGraph = ['Salience', 'Sonar', 'Salience + Sonar', 'Sonar Without Winner Index']
exp = ['TurnAway', 'TurnTowards', 'NonAttentive']
startPoseAttentive = ['>', 'v', '^']
startPoseBrat = ['<', '^', '<']

# Plot trajectory
fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(obstaculosX,obstaculosY, c='m',marker='x', label = 'Objects')

posIniAll = []
posFimAll = []

posIniAllBrat = []
posFimAllBrat = []

j = 1

with open('positionsGroundTruth.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)
posTup = []

i = 0

while i < (tam-1):
	x = float(lines[i])
	y = float(lines[i+1])
	pos = [x, y]#[-y, x]
	posTup.append(pos)
	i = i+2

posX, posY = zip(*posTup)

posIt = [posX[0], posY[0]]
posItF = [posX[len(posX)-1], posY[len(posY)-1]]

posIniAll.append(posIt)
posFimAll.append(posItF)

with open('exploringActionsNumber.txt', 'r') as f:
	actions = f.read()
actions = actions.replace("\n", "")

ax.scatter(posX, posY, c = colorGraphs[c], label = actions + ' actions')

#######################################################################################################
with open('position2.txt', 'r') as f:
	x = f.read()
	lines = x.split()
tam = len(lines)
posTup = []

i = 0

while i < (tam-1):
	x = float(lines[i])
	y = float(lines[i+1])
	pos = [x, y]#[-y, x]
	posTup.append(pos)
	i = i+2

posX, posY = zip(*posTup)

posIt = [posX[0], posY[0]]
posItF = [posX[len(posX)-1], posY[len(posY)-1]]

posIniAllBrat.append(posIt)
posFimAllBrat.append(posItF)

ax.scatter(posX, posY, c = colorGraphs[3], label ='Braitenberg')

######################################################################################################

posXI, posYI = zip(*posIniAll)
posXF, posYF = zip(*posFimAll)

posXIBrat, posYIBrat = zip(*posIniAllBrat)
posXFBrat, posYFBrat = zip(*posFimAllBrat)

ax.scatter(posXF,posYF,c='r',marker='D', s=100,  zorder=3, label = 'End')
ax.scatter(posXI[0],posYI[0],c='k',marker= startPoseAttentive[c], s=100, zorder=3, label = 'Start Pose')

#Brateiberg
ax.scatter(posXIBrat[0],posYIBrat[0],c='k',marker=startPoseBrat[c], s=100, zorder=3)
ax.scatter(posXFBrat,posYFBrat,c='r',marker='D', s=100,  zorder=3)

ax.grid(True)
ax.legend(loc='best')
plt.xlabel("x")
plt.ylabel("y")
plt.title('Trajectory using ' + titleGraph[t])
plt.savefig('Trajectory0'+ str(c+1) + exp[e] + titleGraph[t] + '.pdf')
plt.show()
