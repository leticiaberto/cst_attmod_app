import csv
import numpy as np

file = open('/home/leticia/Development/cst_attmod_app/Experiment_Let/Exps_Robot+BratRob/New_Exploring/actionsAnalysisNew.csv', 'a', newline='')
write = csv.writer(file, delimiter = ",")

stateCalc = ['Salience', 'Sonar', 'Salience + Sonar']
rotate = ['Turn away from Winner', 'Turn towards to Winner']

####################################Define this variables to each experiment###############################################
#Choose mode of calculate the state. The options are in 'stateCalc'
s = 0
#Chosse the classification of experiment. The options are in 'rotate'. Para o NonAttentive usar r = 2
r = 0
#Chose the kind of experiment
names = [stateCalc[s], 'Move Foward', 'Do nothing', rotate[r]]
#names = [stateCalc[s], 'Move Foward', 'Do nothing', 'Turn Left', 'Turn Right']
############################################################################################################################

write.writerow(names)

j = 1

while (j < 10):
	valuesPose = []
	MoveFoward = 0
	TurnAway = 0
	TurnTowards = 0
	TurnLeft = 0
	TurnRight = 0
	DoNothing = 0

	arquivo = open('actionTook0'+str(j)+'.txt', 'r')
	for linha in arquivo:
		if (linha == 'Move Foward\n'):
			MoveFoward += 1
		elif (linha == 'Turn away from Winner\n'):
			TurnAway += 1
		elif (linha == 'Turn towards to Winner\n'):
			TurnTowards += 1
		elif (linha == 'Turn Left\n'):
			TurnLeft += 1
		elif (linha == 'Turn Right\n'):
			TurnRight += 1
		elif (linha == 'Do nothing\n'):
			DoNothing += 1

	valuesPose.append(j)
	valuesPose.append(MoveFoward)
	valuesPose.append(DoNothing)
	if (r == 0):
		valuesPose.append(TurnAway)
	elif (r == 1):
		valuesPose.append(TurnTowards)
	else:
		valuesPose.append(TurnLeft)
		valuesPose.append(TurnRight)

	write.writerow(valuesPose)

	arquivo.close()

	j += 1

write.writerow('')
file.close()
