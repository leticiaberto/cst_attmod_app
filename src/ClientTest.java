
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author leticia
 */import org.json.*;
public class ClientTest {
    public static void main(String[] args)
   {
        String hostName = "localhost";
        int portNumber = 10010;

        try {

            //open a socket
            Socket clientSocket = new Socket(hostName, portNumber);


            BufferedReader in =  new BufferedReader( new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out =  new PrintWriter(clientSocket.getOutputStream(), true);

            System.out.println("Connected");

            Double[][] test2 =   new Double[5][2];
            test2[1][1]= 0.1;
            test2[1][0]= 0.2;
            test2[2][1]= 0.2;
            test2[2][0]= 0.2;
            test2[3][1]= 0.1;
            test2[3][0]= 0.2;
            test2[4][1]= 0.2;
            test2[4][0]= 0.2;
            test2[0][1]= 0.2;
            test2[0][0]= 0.2;

            //out.println(test2);
            out.println(new JSONArray(test2).toString());
            String response;
            while ((response = in.readLine()) != null)
            {
                System.out.println( response );
            }

        }catch (IOException e) {
             System.out.println("Caught Exception: " + e.toString());
          } catch (JSONException ex) { 
            Logger.getLogger(ClientTest.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}

