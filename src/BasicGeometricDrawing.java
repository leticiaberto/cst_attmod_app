
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leticia
 */
public class BasicGeometricDrawing {
    public static void main(String[] args) {
        // Load the native library.
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat img = Imgcodecs.imread("imgReconstruida.png");
         HighGui.namedWindow("image", HighGui.WINDOW_AUTOSIZE);
         HighGui.imshow("image", img);
         HighGui.waitKey();
        new GeometricDrawingRun().run();
    }
}
