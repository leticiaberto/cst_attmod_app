/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author leticia
 */
import java.lang.*;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;


public class Client
{
   //static private List<Integer> camera_data;
   
   private Socket socket = null;
   
   private BufferedReader reader = null;
   private BufferedWriter writer = null;
   
   //private DataOutputStream ToServer = null;
  // private DataInputStream FromServer = null;
   
   // private ObjectOutputStream objectOutput = null;
   //private ObjectInputStream objectInput = null;
   
   public Client(InetAddress address, int port) throws IOException
   {
      socket = new Socket(address, port);
      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
      
      //ToServer = new DataOutputStream(socket.getOutputStream());
      //FromServer = new DataInputStream(socket.getInputStream());
      
      //objectOutput = new ObjectOutputStream(socket.getOutputStream());
      //objectInput =  new ObjectInputStream(socket.getInputStream()); 
      //camera_data = new ArrayList<>(10);
      //for (int i = 0; i < 10; i++) {//o limite foi tirado do tamanho da imagem retornada no vision_sensor (resolução*resolução*3(rgb))
            //camera_data.add(0);
        //}
   }

   public void send(String msg) throws IOException
   {
      writer.write(msg, 0, msg.length());
      writer.flush();
   }
   
   /*public void sendNum(Integer msg) throws IOException
   {
      //ToServer.flush();
       //System.out.println(msg);
      ToServer.write(msg);
      ToServer.flush();
   }
   
    private void sendArray(List<Integer> camera_data) throws IOException {
        //System.out.println(camera_data);
        objectOutput.writeObject(camera_data);
        objectOutput.flush();
         //To change body of generated methods, choose Tools | Templates.
    }*/
   public String recv() throws IOException
   {
      return reader.readLine();
   }
   
   /*private void sendArrayReal(int[] camera_data) throws IOException  {
       objectOutput.writeObject(camera_data);
         //To change body of generated methods, choose Tools | Templates.
       objectOutput.flush();
    }
   
   public Integer recvNum() throws IOException
   {
      return FromServer.readInt();
   }

   public void recvArray() throws IOException, ClassNotFoundException
   {
       System.out.println(objectInput.readObject());
      //return (List<Integer>) objectInput.readObject();
   }
    
    //public void recvArrayReal() throws IOException, ClassNotFoundException
   //{
      // int[] array = (int[]) objectInput.readObject();
       ///for (int i=0; i<array.length; i++) {
         ///   System.out.print(", " + array[i]);
        //}
      // System.out.println(objectInput.readObject());
      //return (List<Integer>) objectInput.readObject();
   //}
   */
   public void clean() throws IOException{
       
        socket.shutdownInput();
        socket.shutdownOutput();
        socket.close();

        //ToServer.flush();
        //ToServer.close();

        //FromServer.close();

        writer.flush();
        writer.close();

        reader.close();   
   }
   public static void main(String[] args) throws ClassNotFoundException
   {
       System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
      
       Mat img = new Mat( 3, 3, CvType.CV_64FC1 );
       int row = 0, col = 0;
       img.put(row ,col, 0, -1, 0, -1, 5, -1, 0, -1, 0 );
       System.out.format(img.dump());
       
      try {
         InetAddress host = InetAddress.getByName("127.0.0.1");
         Client client = new Client(host, 65532);

           //for (int i = 0; i < 10; i++){
            //System.out.format("%d ",(int)image.getArray()[i]);
            //camera_data.set(i, i);    
           //}
        // System.out.println(camera_data);
         
       //int[] values = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        //for (int i = 0; i < values.length; i++) {
            //System.out.print(values[i] + " ");
       // }
        
        
         //for(int i = 0; i < 1; i++){
           //client.send("Hello server.\n");
           client.send(img.dump());
           //client.sendMat(img);
           //String response = client.recvMat();
           //System.out.println("Client received: " + response);
           //client.send(camera_data.toString() + "\n");
           String response = client.recv();
           System.out.println("Client received: " + response);
            
           //client.sendNum(i);
           //Integer responseNum = client.recvNum();
           //System.out.println("Client received: " + responseNum);
            
           
           //client.sendArray(camera_data);
            //client.recvArray();
            //client.sendArray(camera_data);
            //client.recvArray();
            //System.out.println(response);
         //}
         
         client.clean();
      }
      catch (IOException e) {
         System.out.println("Caught Exception: " + e.toString());
      } 
   }

   /*
    private void sendMat(Mat img) throws IOException {
        objectOutput.writeObject(img);
    }

    private String recvMat() throws IOException, ClassNotFoundException {
            return(objectInput.readObject().toString());
   }*/
}
