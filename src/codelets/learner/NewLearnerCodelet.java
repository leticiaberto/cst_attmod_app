/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.learner;

import CommunicationInterface.SensorI;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import attention.Winner;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.Memory;
import br.unicamp.cst.core.entities.MemoryObject;
import br.unicamp.cst.core.entities.RawMemory;
import br.unicamp.cst.core.exceptions.CodeletActivationBoundsException;
import br.unicamp.cst.memory.WorkingStorage;
import codelets.motor.Lock;
import coppelia.FloatWA;
import br.unicamp.cst.learning.QLearning;
import br.unicamp.cst.learning.QLearning;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;
import outsideCommunication.OutsideCommunication;
import outsideCommunication.SonarData;

/**
 *
 * @author leticia
 */
public class NewLearnerCodelet extends Codelet{
    
    private int time_graph;
    private static float CRASH_TRESHOLD = 0.09f;

    private static int MAX_ACTION_NUMBER = 700;

    private static int MAX_EXPERIMENTS_NUMBER = 1000;

    private QLearning ql;
    

    private List winnersList;
    private List saliencyMap;
    private SonarData sonarReadings;
    
    private List sonar_bufferMO;
    //private MemoryObject motorActionMO;
    //private MemoryObject leftMotorMO;
    //private MemoryObject rightMotorMO;
    
    //ActionsList é um MO de Entrada e Saída, logo é necessário atualiza-lo para os demais codelets usarem
    private MemoryObject actionsListMO;
    
    /*Alterações Leticia*/
    private List gt = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
    
    //private ArrayList actionWinnerActual = new ArrayList();
    private List actionsWinnerList = new ArrayList<ArrayList>();
            
    private List<String> actionsList; 
    private List<String> statesList;
    
    private OutsideCommunication oc;
    private int timeWindow;
    private int sensorDimension;
    
    private float vel = 2f;
    
    private double global_reward;
    private int action_number;
    private int experiment_number;
    
    private String mode;
    private int n = 3;//Qntd de features de GroundTruth(gd) que serão armazenadas no MO
    private double episodeReward;
    
    public NewLearnerCodelet (OutsideCommunication outc, int tWindow, int sensDim, String mode) {
//-Xms1746230m
        super();
        time_graph = 0;

        global_reward = 0;

        action_number = 0;

        experiment_number = 1;
        
        episodeReward = 0;
        
        //Itializing
        for(int i = 0; i < n; i++)
            gt.add(0);

        //Se mudar as strings de ações tem que mudar nos codelets de behavior também
       ArrayList<String> allActionsList  = new ArrayList<>(Arrays.asList("Move Foward", "Do nothing", "Turn away from Winner"));
       // ArrayList<String> allActionsList  = new ArrayList<>(Arrays.asList("Move Foward", "Do nothing", "Turn towards to Winner")); 
       // ArrayList<String> allActionsList  = new ArrayList<>(Arrays.asList("Move Foward", "Do nothing", "Turn Left", "Turn Right"));

        // States are 0 1 2 ... 5^8-1. Just Saliency or just Sonar
       ArrayList<String> allStatesList = new ArrayList<>(Arrays.asList(IntStream.rangeClosed(0, (int)Math.pow(5, 8)-1).mapToObj(String::valueOf).toArray(String[]::new)));
    
        // States are 0 1 2 ... 5^16-1. Saliency + Sonar        
	//ArrayList<String> allStatesList = new ArrayList<>(Arrays.asList(LongStream.rangeClosed(0, (long)Math.pow(5, 9)-1).mapToObj(String::valueOf).toArray(String[]::new)));        
       
        // QLearning initialization
        ql = new QLearning();
        ql.setActionsList(allActionsList);
        // learning mode ---> build q table from scratch
        if (mode.equals("learning")) {
        // Initialize QTable to 0
            for (int i=0; i < allStatesList.size(); i ++) {
                for (int j=0; j < allActionsList.size(); j++) {
                    ql.setQ(0, allStatesList.get(i), allActionsList.get(j));

                }
            }
        }
        // exploring mode ---> reloads q table 
        else {
            try {
                ql.recoverQ();
            }
            catch (Exception e) {
                System.out.println("ERROR LOADING QTABLE");
                System.exit(1);
            }
        }


        oc = outc;
        timeWindow = tWindow;
        sensorDimension = sensDim;
        this.mode = mode;
    }

    @Override
    public void accessMemoryObjects() {

        MemoryObject MO;
        MO = (MemoryObject) this.getInput("SALIENCY_MAP");
        saliencyMap = (List) MO.getI();
        MO = (MemoryObject) this.getInput("WINNERS");
        winnersList = (List) MO.getI();
        //MO = (MemoryObject) this.getInput("SONARS");
        //sonarReadings = (SonarData) MO.getI();

        //Leticia
        MO = (MemoryObject) this.getInput("GTMO");
        gt = (List) MO.getI();

        MO = (MemoryObject) this.getInput("SONAR_BUFFER");
        sonar_bufferMO = (List)MO.getI();
        
        //Verificar se a posição e orientação de GroundTruth estão vindo corretas do MO de GT
        /*System.out.println("gt List Learner - position");
        float [] position = ((FloatWA) gt.get(0)).getArray();
        for(int i = 0; i < position.length; i++)
            System.out.println(position[i]);
        /*
        System.out.println("gt List Learner - orientation");
        float [] orientation2 = ((FloatWA) gt.get(1)).getArray();
        for(int i = 0; i < orientation2.length; i++)
            System.out.println(orientation2[i]);
        */

        /*motorActionMO = (MemoryObject) this.getOutput("MOTOR");
        leftMotorMO = (MemoryObject) this.getOutput("L_M_SPEED");
        rightMotorMO = (MemoryObject) this.getOutput("R_M_SPEED");*/

        //MO = (MemoryObject) this.getOutput("ACTIONS");
        //actionsList = (List) MO.getI();

        MO = (MemoryObject) this.getOutput("STATES");
        statesList = (List) MO.getI();

        
        actionsListMO = (MemoryObject) this.getOutput("ACTIONS");
        //actionsList = (List) MO.getI();
        
        //ArrayList name;
            //name = ((ArrayList) actionsWinnerList.get(0));
            //String x = (String) name.get(0);
            //System.out.println("Action vet: "+x);

    }

    @Override
    public void calculateActivation() {
            // TODO Auto-generated method stub

    }

    public static Object getLast(List list) {
            if (list.size() != 0) {
                    return list.get(list.size()-1);
            }
            return null;
    }

	
	
    @Override
    public void proc() {

        try {
            Thread.sleep(90);//Estava 90
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
                
        String state = "-1";

        if (!saliencyMap.isEmpty() && !winnersList.isEmpty()) {

            Winner lastWinner = (Winner) winnersList.get(winnersList.size() - 1);
            Integer winnerIndex = lastWinner.featureJ;
            
            //System.out.println("Winner LEARNER: "+winnerIndex);
            
            //if (!actionsList.isEmpty() && mode.equals("learning")) {
            if (!actionsWinnerList.isEmpty() && mode.equals("learning")) {
                // Find reward of the current state, given previous  winner 
                check_stop_experiment(mode);
                //Double reward = 1d; //-- Carol
                Double reward = 0d;
                
                // Gets last action taken
                //String lastAction = actionsList.get(actionsList.size() - 1); 
                String lastAction = (String) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(0);
                // Gets last state that was in
                String lastState = statesList.get(statesList.size() - 1);
                
                switch (lastAction) {
                    case "Move Foward":
                        reward = 1d;
                        break;
                    case "Do nothing":
                        reward = 0d;
                        break;
                    case "Turn towards to Winner":
                        reward = 0.5d;
                        break;
                    case "Turn away from Winner":
                        reward = 0.5d;
                        //System.out.println("Reward: "+reward);
                        break;
                    case "Turn Left":
                        reward = 0.5d;
                        break;
                    case "Turn Right":
                        reward = 0.5d;
                        break;
                    default:
                        break;
                }
                
                global_reward += reward;
                episodeReward += reward;
                
                //System.out.println("Global Reward: "+global_reward);
                //System.out.println("Episode Reward: "+episodeReward);
                
                // Updates QLearning table
                ql.update(lastState, lastAction, reward);
            }

            //Escolher de acordo com o modo a ser rodado o experimento
            state = getStateFromSalMapOnlySalience();
            //state = getStateFromSalMapOnlySonar();
            //state = getStateFromSalMapSalienceSonar();
            
            statesList.add(state);


            // Selects new best action to take
            String actionToTake = ql.getAction(state);
            //actionsList.add(actionToTake);// Contêm as ações


            // actionsListMO.setI(actionsList);  
             
            action_number++;
            
            /*
                Contem a ação ATUAL vencedora e qual foi a feature vencedora
                actionWinnerActual(0) = ação escolhida
                actionWinnerActual(1) = feature vencedora (vinda do WinnerMO)
            */
            ArrayList actionWinnerActual = new ArrayList();
            actionWinnerActual.add(actionToTake);        
            actionWinnerActual.add(winnerIndex); 
            
            //Ações e features vencedoras ao longo do tempo
            actionsWinnerList.add(actionWinnerActual);
            
            actionsListMO.setI(actionsWinnerList);
            
            //System.out.println("size learner " + actionsWinnerList.size() + " " + actionsWinnerList);
            //ArrayList name;
            //name = ((ArrayList) actionsWinnerList.get(0));
            //String x = (String) name.get(0);
            //System.out.println("Action vet: "+x);
            
            //System.out.println("Action LEARNER: "+actionToTake);
            
            //Print the action choose in exploring
            
            printToFileActionExploring(actionToTake);
        }
        //System.out.println("actions: " + action_number);
        time_graph = printToFile(0, state, "states.txt", time_graph,0, true);
        
        if (mode.equals("exploring")) {
            //oc.pioneer_position.getData();//Carol - pegava o GT atual do robô, não o do MO que foi utilizado no ciclo cognitivo
            if(!gt.isEmpty()){
                FloatWA positionGT = ((FloatWA) gt.get(0));
                printToFilePosition(positionGT, "positionsGroundTruth.txt");
            }
            check_stop_experiment(mode);           
        }

    }
	
	
	
    public void check_stop_experiment(String mode) {

        boolean crashed = false;
        /* -- Versão da Carol - lia o valor do MO sonar, mas ele naão esta conectado com esse codelet. 
        O correto é ler do sonar_Buffer (refeito corretamente abaixo desse trecho comentado
        if (!sonarReadings.sonar_readings.isEmpty()){
            // check if crashed anything
            for (int i=0; i < sensorDimension; i++) {
                    float reading = sonarReadings.sonar_readings.get(i);
                    if ( reading != 0.0f & reading < CRASH_TRESHOLD) {
                            System.out.println("Crashed something!");
                            crashed = true;
                            break;
                    } 
            }
        }*/
         if (!sonar_bufferMO.isEmpty()){
            MemoryObject sonarDataMO;
            sonarDataMO = (MemoryObject) sonar_bufferMO.get(sonar_bufferMO.size() -1);
             
             SonarData sonarData1;
             sonarData1 =  (SonarData) sonarDataMO.getI();
             
             Float f1;
            
            // check if crashed anything
            for (int i=0; i < sensorDimension; i++) {
                f1 = sonarData1.sonar_readings.get(i);
                if (f1 != 0.0f & f1 < CRASH_TRESHOLD) {
                    System.out.println("Crashed something!");
                    crashed = true;
                    break;
                } 
            }
        }
        if (mode.equals("exploring") && crashed) {
            //Test to see if prints the robots position closer to obstacle
            if(!gt.isEmpty()){
                FloatWA positionGT = ((FloatWA) gt.get(0));
                printToFilePosition(positionGT, "positionsGroundTruth.txt");
            }
            printToFileActionsExploring("exploringActionsNumber.txt");
            oc.stopSimulation();
            System.exit(0);
        }
        else if (mode.equals("learning") && (action_number >= MAX_ACTION_NUMBER || crashed)) {
                System.out.println("Max number of actions or crashed");
                experiment_number = printToFile(episodeReward, global_reward, "rewards.txt", experiment_number, action_number,false);
                action_number = 0;
                episodeReward = 0;  
                System.out.println("Experiment Number: "+experiment_number);
                if (experiment_number > MAX_EXPERIMENTS_NUMBER) {
                        ql.storeQ();
                        System.exit(0);
                }
                oc.resetCuboids();//Arruma os obstaculos do ambiente que podem ter sido modificados com a colisão do robô
                oc.pioneer_position.resetData();//reinicia com robô em outra posição
                
                try {
            Thread.sleep(500);
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        }


    }
	
    // Normalize and transform a salience map into one state
    // Normalized values between 0 and 1 can be mapped into 0, 1, 2, 3 or 4
    // Them this values are computed into one respective state
    public String getStateFromSalMapOnlySalience() {

        //If running the experiment ONLY with sonar, comment this part
        
        /************************* SALIENCY MAP ******************************/
        ArrayList<Float> lastLine;
        // Getting just the last entry (current sal map)
        lastLine = (ArrayList<Float>) saliencyMap.get(saliencyMap.size() -1);

        // For normalizing readings between 0 and 1 before transforming to state 
        Float max = Collections.max(lastLine);
        Float min = Collections.min(lastLine);		

        Integer discreteVal = 0;
        //Integer stateVal = 0;
        
        //Double stateVal = 0.0;//Use just with Saliency + Sonar experiment
        long stateVal = 0;
        
        for (int i=0; i < sensorDimension; i++) {
                // Normalizing value
                Float normVal = (lastLine.get(i)-min)/(max-min);

                // Getting discrete value
                if (normVal <= 0.2) {
                        discreteVal = 0;
                }
                else if (normVal <= 0.4) {
                        discreteVal = 1;
                }
                else if (normVal <= 0.6) {
                        discreteVal = 2;
                }
                else if (normVal <= 0.8) {
                        discreteVal = 3;
                }
                else if (normVal <= 1) {
                        discreteVal = 4;
                }

                // Getting state from discrete value
                stateVal += (long) Math.pow(5, i)*discreteVal; 
        }
        
       return Long.toString(stateVal);
    }
    
    public String getStateFromSalMapOnlySonar() {
        
        /************************* SONAR VALUE ******************************/    
        // Getting just the last entry (current sonar reading that is in buffer)
        
        MemoryObject sonarDataMO;
        sonarDataMO = (MemoryObject) sonar_bufferMO.get(sonar_bufferMO.size() -1);
             
        SonarData sonarData1;
        sonarData1 =  (SonarData) sonarDataMO.getI();
             
          // For normalizing readings between 0 and 1 before transforming to state 
        Float maxSonar = 1f;
        Float minSonar = 0f;		
        
        Integer discreteVal = 0;
        long stateVal = 0;
        
        for (int i=0; i < sensorDimension; i++) {
                // Normalizing value
                Float normVal = (sonarData1.sonar_readings.get(i)-minSonar)/(maxSonar-minSonar);

                // Getting discrete value
                if (normVal <= 0.2) {
                        discreteVal = 0;
                }
                else if (normVal <= 0.4) {
                        discreteVal = 1;
                }
                else if (normVal <= 0.6) {
                        discreteVal = 2;
                }
                else if (normVal <= 0.8) {
                        discreteVal = 3;
                }
                else if (normVal <= 1) {
                        discreteVal = 4;
                }
            
                //Experiment only with Sonar
                stateVal += (long) Math.pow(5, i)*discreteVal; 
        }

       return Long.toString(stateVal);
    }
    
    public String getStateFromSalMapSalienceSonar() {
     
        /************************* SALIENCY MAP ******************************/
        ArrayList<Float> lastLine;
        // Getting just the last entry (current sal map)
        lastLine = (ArrayList<Float>) saliencyMap.get(saliencyMap.size() -1);

        // For normalizing readings between 0 and 1 before transforming to state 
        Float max = Collections.max(lastLine);
        Float min = Collections.min(lastLine);		

        Integer discreteVal = 0;

        long stateVal = 0;
        
        for (int i=0; i < sensorDimension; i++) {
                // Normalizing value
                Float normVal = (lastLine.get(i)-min)/(max-min);

                // Getting discrete value
                if (normVal <= 0.2) {
                        discreteVal = 0;
                }
                else if (normVal <= 0.4) {
                        discreteVal = 1;
                }
                else if (normVal <= 0.6) {
                        discreteVal = 2;
                }
                else if (normVal <= 0.8) {
                        discreteVal = 3;
                }
                else if (normVal <= 1) {
                        discreteVal = 4;
                }

                // Getting state from discrete value
                stateVal += (long) Math.pow(5, i)*discreteVal; 

        }
        
         
        /************************* SONAR VALUE (COMBINED SENSORS) USED IN SALIENCY + SONAR ******************************/    
        
        MemoryObject sonarDataMO;
        sonarDataMO = (MemoryObject) sonar_bufferMO.get(sonar_bufferMO.size() -1);
             
        SonarData sonarData1;
        sonarData1 =  (SonarData) sonarDataMO.getI();
        
        long inter = 0;
        for(int i = 2, j = 0; i < 6; i=i+2, j++){
            if(sonarData1.sonar_readings.get(i) <= 0.5 || sonarData1.sonar_readings.get(i+1) <=0.5)
                discreteVal = 1;
            else
                discreteVal = 0;
            //inter += (long) Math.pow(2, j)*discreteVal;//Binário para decimal
            inter += (long) Math.pow(2, j)*discreteVal;
        }
        //nesse caso pode-se usar sensorDimension, mas dependendo tem que usar outra variavel para guardar o valor correto
        stateVal += (long) Math.pow(5, sensorDimension)*inter;//nao precisa trocar de base, pois o maximo valor dado aqui em decimal é igual na base 5
        //Quando utilizavamos tres sensores do sonar
        /*
        for(int i = 0, j = 0; i < 7; i = i+3, j++){
            if(sonarData1.sonar_readings.get(i) <= 0.5 || sonarData1.sonar_readings.get(i+1) <=0.5)
                discreteVal = 1;
            else
                discreteVal = 0;
            // Getting state from discrete value
            inter += (long) Math.pow(2, j)*discreteVal;//Binário para decimal
        }*/
        
        //Passar o valor da base 10 (resultado acima) para a base 5
       
        /*int sBase = 10; // Source Base  
        int dBase = 5; //Dest Base
		
	String b = Integer.toString(Integer.parseInt(Long.toString(inter), sBase),dBase);
            
        //System.out.println(b.length());
        //System.out.println(b);
        if(b.length() == 2){
            stateVal += (long) Math.pow(5, sensorDimension)*Integer.parseInt(b.substring(b.length()-1));//Menos significativo
            stateVal += (long) Math.pow(5, sensorDimension+1)*Integer.parseInt(b.substring(b.length()-2,b.length()-1));//Mais significativo
        }
        else{
            stateVal += (long) Math.pow(5, sensorDimension+1)*Integer.parseInt(b.substring(b.length()-1));
        }*/
        //return stateVal.toString();
        //System.out.println("antes");
       return Long.toString(stateVal);
    }
		
	
    private int printToFile(double episodeReward, Object object,String filename, int counter, int action_number, boolean check){

        if (!check || counter < MAX_EXPERIMENTS_NUMBER) {
            try(FileWriter fw = new FileWriter(filename,true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                if(filename.equals("rewards.txt"))
                    out.println(counter+" "+ action_number + " " + episodeReward + " " +object );
                else
                    out.println(counter+" "+object );

                out.close();
                return ++counter;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return counter;

    }
    
    private void printToFilePosition(FloatWA position,String filename) {
        try(FileWriter fw = new FileWriter(filename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(position.getArray()[0] + " " + position.getArray()[1]);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void printToFileActionsExploring(String filename) {
        try(FileWriter fw = new FileWriter(filename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
            {
                out.println(action_number);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void printToFileActionExploring(String actionToTake) {
        try(FileWriter fw = new FileWriter("actionTook.txt",true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
            {
                out.println(actionToTake);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
        		

}

