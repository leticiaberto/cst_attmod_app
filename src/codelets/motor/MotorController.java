/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.motor;

import CommunicationInterface.MotorI;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author leticia
 */
public class MotorController extends Codelet{
    
    private MemoryObject motorActionMO;
    private MemoryObject rm_speed_MO, lm_speed_MO; 
    private MotorI rm, lm;
    
    
    public MotorController(MotorI rmo, MotorI lmo){
    	super();
        rm = rmo;
        lm = lmo;
    }

    @Override
    public void accessMemoryObjects() {
        motorActionMO = (MemoryObject) this.getInput("MOTOR");
        rm_speed_MO = (MemoryObject) this.getInput("R_M_SPEED");
        lm_speed_MO = (MemoryObject) this.getInput("L_M_SPEED");
    }

    @Override
    public void calculateActivation() {

    }

    @Override
    public void proc() {
    	try {
            Thread.sleep(50);//Estava 100. O tempo esta em milisegundos (2000 = 2s)
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    	
    	String action = (String) motorActionMO.getI();
    	rm.setSpeed((float) rm_speed_MO.getI());
        lm.setSpeed((float) lm_speed_MO.getI());  
    }//fim proc

}
