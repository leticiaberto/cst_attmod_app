/*
 * /*******************************************************************************
 *  * Copyright (c) 2012  DCA-FEEC-UNICAMP
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the GNU Lesser Public License v3
 *  * which accompanies this distribution, and is available at
 *  * http://www.gnu.org/licenses/lgpl.html
 *  * 
 *  * Contributors:
 *  *     K. Raizer, A. L. O. Paraense, R. R. Gudwin - initial API and implementation
 *  ******************************************************************************/
    
package codelets.motor;

/**
 *
 * @author leandro
 */

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import CommunicationInterface.MotorI;
import br.unicamp.cst.core.entities.MemoryContainer;
import org.json.JSONException;
import org.json.JSONObject;

public class MotorCodelet extends Codelet {
    
    private MemoryObject motorActionMO;
    private MemoryObject rm_speed_MO, lm_speed_MO; 
    private MotorI rm, lm;
    
    private MemoryContainer actionMO;
    
    private int MOVEMENT_TIME = 2000; // 2 seconds
    
    public MotorCodelet(MotorI rmo, MotorI lmo){
    	super();
        rm = rmo;
        lm = lmo;
    }

    @Override
    public void accessMemoryObjects() {
        //motorActionMO = (MemoryObject) this.getInput("MOTOR");
        //rm_speed_MO = (MemoryObject) this.getInput("R_M_SPEED");
        //lm_speed_MO = (MemoryObject) this.getInput("L_M_SPEED");
        
        actionMO = (MemoryContainer)this.getInput("BEHAVIOR");
    }

    @Override
    public void calculateActivation() {

    }

    @Override
    public void proc() {
    	try {
            Thread.sleep(110);//Estava 100. O tempo esta em milisegundos (2000 = 2s)
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
    	/* Versão da Caroline - utilizando LeftMO e RightMO que eram setados nos LearnerCodelet
    	String action = (String) motorActionMO.getI();
    	rm.setSpeed((float) rm_speed_MO.getI());
        lm.setSpeed((float) lm_speed_MO.getI());
    	*/
        
        //Utlizando a mensagem em JSON lida do Container para realizar a ação
        String comm = (String) actionMO.getI();
        if (comm == null) comm = "";
		
        if(!comm.equals("") ){

            try {
                JSONObject command = new JSONObject(comm);
                if (command.has("ACTION")) {
                    
                    String action = command.getString("ACTION");
                    
                    double leftSpeed = command.getDouble("LeftMO");
                    double rightSpeed = command.getDouble("RightMO");
                    //System.out.println("Action MOTOR: " + action); 
                    
                    switch (action) {
                        case "Move Foward":
                            //System.out.println("Action MOTOR: Move Foward");
                            //double leftSpeed = command.getDouble("LeftMO");
                            //double rightSpeed = command.getDouble("RightMO");
                            
                            //rm.setSpeed((float) rightSpeed);
                            //lm.setSpeed((float) leftSpeed);
                            break;
                        case "Turn away from Winner":
                            //System.out.println("Action MOTOR: Turn away from Winner");
                            //double leftSpeed = command.getDouble("LeftMO");
                            //double rightSpeed = command.getDouble("RightMO");
                            
                            //rm.setSpeed((float) rightSpeed);
                            //lm.setSpeed((float) leftSpeed);
                            break;
                        case "Turn towards to Winner":
                            //System.out.println("Action MOTOR: Turn towards to Winner");
                            //double leftSpeed = command.getDouble("LeftMO");
                            //double rightSpeed = command.getDouble("RightMO");
                            
                            //rm.setSpeed((float) rightSpeed);
                            //lm.setSpeed((float) leftSpeed);
                            break;
                        case "Do nothing":
                            //System.out.println("Action MOTOR: Do nothing");
                            //double leftSpeed = command.getDouble("LeftMO");
                            //double rightSpeed = command.getDouble("RightMO");
                            
                            //rm.setSpeed((float) rightSpeed);
                            //lm.setSpeed((float) leftSpeed);  
                            break;
                        default:
                            break;
                    }
                    rm.setSpeed((float) rightSpeed);
                    lm.setSpeed((float) leftSpeed);
                }	
            } catch (JSONException e) {e.printStackTrace();}
        }
        
    }//fim proc
}
