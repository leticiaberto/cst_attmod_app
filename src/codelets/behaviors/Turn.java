/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import coppelia.FloatWA;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author leticia
 */
public class Turn extends Codelet{
    
    private Integer winnerIndex;
    //private List<String> actionsList;//List com as ações do MO resultantes do Learner Codelet
    private List actionsWinnerList = new ArrayList<ArrayList>();
    private String actionsList;
   
    private MemoryContainer behaviorMO;
    
    private int indice = -1;
    private double eval;
    
    private List gt = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
    private int n = 3;//Qntd de features de GroundTruth(gd) que serão armazenadas no MO
    
    //private Float leftSpeed;
    //private Float rightSpeed;
    
    private double linearVelocity = 0d;
    
    private ArrayList<Double> SonarAngles;
    
    //Variaveis para o controlador P
    private double error;
    private double Kp_angle;
    
    private double wheels_distance = 0.381;//0.36205
    private double wheel_radius = 0.0975;
    
    private int tamActionMO;
    private String action;
    
    public Turn(){
        //Itializing
        for(int i = 0; i < n; i++)
            gt.add(0); 
        
        // Angulo do deslocamento de cada sonar em relação ao PioneerP3dx
        SonarAngles = new ArrayList<Double>(){ 
            { 
                add(0.925025);    //sonar 1   Radianos: 0.925025      Graus: 53
                add(0.715585);    //sonar 2   Radianos: 0.715585      Graus: 41
                add(0.401426);    //sonar 3   Radianos: 0.401426      Graus: 23
                add(0.139626);     //sonar 4   Radianos: 0.139626      Graus: 8
                add(-0.139626);    //sonar 5   Radianos: -0.139626    Graus: -8
                add(-0.401426);   //sonar 6   Radianos: -0.401426     Graus: -23
                add(-0.715585);   //sonar 7   Radianos: -0.715585     Graus: -41
                add(-0.92502);   //sonar 8   Radianos: -0.925025     Graus: -53
            } 
        }; 
        
      
        Kp_angle = 8;
        
        tamActionMO = 0;
        action = "Turn towards to Winner";//padrão
    }
   
    @Override
    public void accessMemoryObjects() {
        MemoryObject MO;
        
        MO = (MemoryObject) this.getInput("ACTIONS");
        actionsWinnerList = (List) MO.getI();
        
        behaviorMO = (MemoryContainer)this.getOutput("BEHAVIOR");
        
        //Leticia
        MO = (MemoryObject) this.getInput("GTMO");
        gt = (List) MO.getI();

    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        try {
            Thread.sleep(100);//Estava 50
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
        
        eval = 0.0;
        //leftSpeed = 0f;
        //rightSpeed = 0f;

        
        if (!actionsWinnerList.isEmpty()){
                        
            int sizeAct = actionsWinnerList.size();
            //System.out.println("size turn " + sizeAct + " " + actionsWinnerList);
            //Significa que o Learner colocou uma ação nova no MO. 
            /*Serve para dar prioridade ao learning, e também evitar comportamento indesejado se 
            esse codelet estiver mais rapido*/
            if(sizeAct != tamActionMO){
                tamActionMO = sizeAct;
                //New Action List, contem nome e indice vencedora
                String actionToTake = (String) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(0); 
                //System.out.println("Action turn: "+actionToTake);
                if (actionToTake.equals("Turn towards to Winner")){
                    eval = 1.0;  
                    //System.out.println("Turn towards to Winner " + eval);
                    
                    //Modo antigo
                    //float diff = calcDifRobotWinner();
                    /*				
                    if (diff > 0) {// Target is on the right of robot: rotate left motor
                        System.out.println("Winner on the right! (diff POS). Seeting left speed");
                        leftSpeed = 2f;
                        //leftMotorMO.setI(2f);
                        //rightMotorMO.setI(0f);
                    }
                    else if (diff < 0) {// Target is on the left of robot: rotate right motor
                            System.out.println("Winner on the left! (diff NEG). Seeting right speed");
                            rightSpeed = 2f;
                            //leftMotorMO.setI(0f);
                            //rightMotorMO.setI(2f);
                    }
                    else {//Não precisa virar
                            //leftMotorMO.setI(0f);
                            //rightMotorMO.setI(0f);
                    }   

                    //JSONMessageToMO2(actionToTake);
                    //System.out.println("turn");*/
                    
                    PController(actionToTake);
                }//fim "Turn towards to Winner"

                else if(actionToTake.equals("Turn away from Winner")){
                    eval = 1.0;  
                    //action = "Turn away from Winner";

                    //float diff = calcDifRobotWinner();
                    /*			
                    if (diff > 0) {// winner on the right of robot -> turn lef
                        System.out.println("Winner on the right! (diff POS). Seeting right speed");
                        rightSpeed = 2f;
                        //leftMotorMO.setI(0f);
                        //rightMotorMO.setI(2f);

                    }
                    else if (diff < 0) {// winner on the left of robot -> turn right
                        System.out.println("Winner on the left! (diff NEG). Seeting left speed");
                            leftSpeed = 2f;
                            //leftMotorMO.setI(2f);
                            //rightMotorMO.setI(0f);

                    }
                    else {//Não precisa virar
                            //leftMotorMO.setI(0f);
                            //rightMotorMO.setI(0f);
                    }   
                    //JSONMessageToMO2(actionToTake);*/

                    PController(actionToTake);
                }//fim "Turn away from Winner"
                else if (actionToTake.equals("Turn Left") || actionToTake.equals("Turn Right")){
                    eval = 1.0;  
                    turnFixo(actionToTake);
                }
                else
                    JSONMessageToMO(action, 0, 0);
                //JSONMessageToMO2(actionToTake);
                //PController(action);
            }
        }
          
    }
    
    
    
    void JSONMessageToMO(String actionToTake, float left_velocity , float right_velocity){
        //System.out.println("TURN " + eval);
        
        JSONObject message = new JSONObject();
        try {
            message.put("ACTION", actionToTake);
            message.put("LeftMO", left_velocity);
            message.put("RightMO",right_velocity);

            if(indice == -1)
                indice = behaviorMO.setI(message.toString());
            else
                behaviorMO.setI(message.toString(), eval, indice);
        } catch (JSONException ex) {
            Logger.getLogger(MoveForward.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
                
    public void PController(String actionToTake){
        
        double u;//velocidade angular
        float left_velocity;
        float right_velocity;
            
        //get robot orientation
        float [] robotOrientation = ((FloatWA) gt.get(1)).getArray();
        float pioneer_orientation = robotOrientation[2]; 
            
        //Get The sonar angle referring the winner index 
        Integer winnerIndexSonar = (Integer) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(1);
        
        //Calculate the angle that the robot needs to turn
        Double targetAngle = pioneer_orientation + SonarAngles.get(winnerIndexSonar);
        
        //System.out.println("robotOrientation: " + pioneer_orientation);
        //System.out.println("winnerIndex: " + winnerIndexSonar);
        //System.out.println("winnerAngleSonar: " + SonarAngles.get(winnerIndexSonar));
        //System.out.println("target: " + targetAngle);
        
                
        int sizeAct;
        //boolean sameAction = false;
        //String actionToTakeNew = null;
       
        if (actionToTake.equals("Turn away from Winner")){
               targetAngle = -targetAngle;//Para virar ao contrário da direção do target
        }
        
        //System.out.println("target: " + targetAngle); 
        
        while(true){        
            
            error = targetAngle - pioneer_orientation;
            //System.out.println("error: " + error);
            
            //erro e calculo do angulo
            u = (Kp_angle * error);
            
            //System.out.println("u: " + u);
            
            if(Math.abs(error) <= 0.0349066){//o quanto vou aceitar de distancia do angulo objetivo (2 graus = 0.0349066 radianos)
                break;
            }
            
            left_velocity =  (float) ((2 * linearVelocity - wheels_distance * u) / (2 * wheel_radius));
            right_velocity = (float) ((2 * linearVelocity + wheels_distance * u) / (2 * wheel_radius));
            
            JSONMessageToMO(actionToTake, left_velocity, right_velocity);
            
            //get robot orientation
            robotOrientation = ((FloatWA) gt.get(1)).getArray();
            pioneer_orientation = robotOrientation[2];
            
            /*Externo ao PID
            É utilizado para parar de virar caso o LEARNER dê outra ação, logo a 
            prioridade é realizar ações do LEARNER, mesmo que não tenha terminado a atual.
            */
            sizeAct = actionsWinnerList.size();
            
            /*Caso esteja virando, mas o learner decida nova ação com o comando de virar novamente
            Se não colocar isso, esse comportamente pode ser perdido, pois a thread poderia demorar para
            ler esse comando e o learner poderia colocar outro comando novo e diferente.
            */
            
            if(sizeAct != tamActionMO){
                /*tamActionMO = sizeAct;
                actionToTakeNew = (String) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(0); 
                
                if (!(actionToTakeNew.equals("Turn towards to Winner") || actionToTakeNew.equals("Turn away from Winner"))){
                   resetPIDVars();
                   eval = 0;
                   break;
                }
                else{
                    resetPIDVars();
                    sameAction = true;
                    break;
                }
                /*if (actionToTakeNew.equals("Turn towards to Winner") || actionToTakeNew.equals("Turn away from Winner")){
                    sameAction = true;
                    //resetPIDVars();
                    eval = 1.0;//Para caso a ação anterior nao seja as dessa classe, tem que dar esse eval como maior agora
                    break;
                }*/
                //resetPIDVars();
                //break;*/
                break;
            }
        }
        //if(sameAction)
            //PIDController(actionToTakeNew);
        //return u;
    }
    
    public void turnFixo(String actionToTake){
        if(actionToTake.equals("Turn Left"))
            //turn -15 graus. Setar velocidade roda direita
            JSONMessageToMO(actionToTake, 0, 2); 
        else
            //turn 15 graus. Setar velocidade roda esquerda
            JSONMessageToMO(actionToTake, 2, 0);    
    }
    
    /*
    void JSONMessageToMO2(String actionToTake){
        
        JSONObject message = new JSONObject();
        try {
            message.put("ACTION", actionToTake);
            message.put("LeftMO", leftSpeed);
            message.put("RightMO", rightSpeed);

            if(indice == -1)
            indice = behaviorMO.setI(message.toString());
        else
            behaviorMO.setI(message.toString(), eval, indice);
        
        } catch (JSONException ex) {
            Logger.getLogger(MoveForward.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    private float calcDifRobotWinner(){
        float diff = 0;

        if (!actionsWinnerList.isEmpty()){
            //get robot orientation
            float [] robotOrientation = ((FloatWA) gt.get(1)).getArray();
            float pioneer_orientation = robotOrientation[2];

            //Winner lastWinner = (Winner) winnersList.get(winnersList.size() - 1);
            //Integer winnerIndex = lastWinner.featureJ;
            
            //winnerIndex = (Integer) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(1);
            
            //System.out.println("Winner TURN: "+winnerIndex);
            
            // get winner orientation
            float[] sonarOrientation = ((ArrayList<float[]>) gt.get(2)).get(winnerIndex);
            float winner_orientation = sonarOrientation[2];
            
            //Usando posicionamento do Sonar 
            Integer winnerIndexSonar = (Integer) ((ArrayList)actionsWinnerList.get(actionsWinnerList.size()-1)).get(1);
            //Calculate the angle that the robot needs to turn
            //diff = pioneer_orientation - SonarAngles.get(winnerIndexSonar);
            
            //Calcular a orientação do robô em relação ao sonar da feature vencedora
            //diff = pioneer_orientation - winner_orientation;
        }
        return diff;
    }*/
}
