/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.behaviors;

import attention.Winner;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import coppelia.FloatWA;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import outsideCommunication.OutsideCommunication;

/**
 *
 * @author leticia
 */
public class PIDController extends Codelet{
    
    //Variaveis para PID
    private int count_pid;
    private double old_error;
    private double i_error;
    private double diff_error;
    private double error;

    private double Kp_angle;
    private double Ki_angle;
    private double Kd_angle;
    
    private MemoryObject motorActionMO;
    private MemoryObject leftMotorMO;
    private MemoryObject rightMotorMO;
    
    private ArrayList<Double> SonarAngles;
    
    private int n = 3;//Qntd de features de GroundTruth(gd) que serão armazenadas no MO
    
    private List gt = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
    
    private double wheels_distance = 0.381;
    private double wheel_radius = 0.0975;
    private double linearVelocity = 0d;
    
    private List winnersList;
    
    private int time_graph;
    private double i;
    
    private OutsideCommunication oc;
    
    public PIDController(OutsideCommunication outc){
        
         //Itializing
        for(int i = 0; i < n; i++)
            gt.add(0); 
        
        // Angulo do deslocamento de cada sonar em relação ao PioneerP3dx
        SonarAngles = new ArrayList<Double>(){ 
            { 
                add(0.925025);    //sonar 1   Radianos: 0.925025      Graus: 53
                add(0.715585);    //sonar 2   Radianos: 0.715585      Graus: 41
                add(0.401426);    //sonar 3   Radianos: 0.401426      Graus: 23
                add(0.139626);     //sonar 4   Radianos: 0.139626      Graus: 8
                add(-0.139626);    //sonar 5   Radianos: -0.139626    Graus: -8
                add(-0.401426);   //sonar 6   Radianos: -0.401426     Graus: -23
                add(-0.715585);   //sonar 7   Radianos: -0.715585     Graus: -41
                add(-0.92502);   //sonar 8   Radianos: -0.925025     Graus: -53
            } 
        }; 
        
        resetPIDVars();
        
        Kp_angle = 8;//0.01
        Ki_angle = 0;//0.001
        Kd_angle = 0;//0.1
        
        time_graph = 1;
        
        oc = outc;
        
        i = 1;
    }
    
    
    public void accessMemoryObjects() {
        motorActionMO = (MemoryObject) this.getOutput("MOTOR");
        leftMotorMO = (MemoryObject) this.getOutput("L_M_SPEED");
        rightMotorMO = (MemoryObject) this.getOutput("R_M_SPEED");
        
        MemoryObject MO;
        
        //Leticia
        MO = (MemoryObject) this.getInput("GTMO");
        gt = (List) MO.getI();
        
        MO = (MemoryObject) this.getInput("WINNERS");
        winnersList = (List) MO.getI();
        
    }
    
    public void PIDController(String actionToTake){
        
        double u;
        
        //get robot orientation
        float [] robotOrientation = ((FloatWA) gt.get(1)).getArray();
        float pioneer_orientation = robotOrientation[2]; 
            
        //Get The sonar angle referring the winner index 
        Winner lastWinner = (Winner) winnersList.get(winnersList.size() - 1);
        Integer winnerIndex = lastWinner.featureJ;
        
        //Calculate the angle that the robot needs to turn
        Double targetAngle = pioneer_orientation + SonarAngles.get(winnerIndex);
        
        System.out.println("robotOrientation: " + pioneer_orientation);
        System.out.println("winnerIndex: " + winnerIndex);
        System.out.println("winnerAngleSonar: " + SonarAngles.get(winnerIndex));
        
        old_error = targetAngle - pioneer_orientation;//In the first time the diff_error = error
        //old_error = 0;
               
        if (actionToTake.equals("Turn away from Winner")){
               targetAngle = -targetAngle;//Para virar ao contrário da direção do target
        }
        
        System.out.println("target: " + targetAngle); 
        targetAngle = 1.5708;
        while(true){        
            
            error = targetAngle - pioneer_orientation;
            System.out.println("robotOrientation: " + pioneer_orientation);
            System.out.println("error: " + error);
            
            if(Math.abs(error) <= 0.0349066){//o quanto vou aceitar de distancia do angulo objetivo (2 graus = 0.0349066 radianos)
                resetPIDVars();
                leftMotorMO.setI(0f);
                rightMotorMO.setI(0f);
                break;
            }
            
            //janela de acúmulo do erro = 10, depois zera janela (Saturation caused by integration)
            if (count_pid == 10){
                i_error = 0;
                count_pid = 0;
            }

            //erro e calculo do angulo
            i_error = i_error + error;
            diff_error = error - old_error;
            u = (Kp_angle * error)  + (Kd_angle * diff_error) + (Ki_angle * i_error);
            old_error = error;
            
            System.out.println("u: " + u);
            
            //incrementa contagem da janela de acúmulo
            count_pid = count_pid + 1;
            
            float left_velocity =  (float) ((2 * linearVelocity - wheels_distance * u) / (2 * wheel_radius));
            float right_velocity = (float) ((2 * linearVelocity + wheels_distance * u) / (2 * wheel_radius));
        
            System.out.println("LeftVel " + left_velocity);
            System.out.println("RightVel " + right_velocity);
            
            leftMotorMO.setI(left_velocity);
            rightMotorMO.setI(right_velocity);
            
            //get robot orientation
            robotOrientation = ((FloatWA) gt.get(1)).getArray();
            pioneer_orientation = robotOrientation[2]; 
            
            
            printToFile(pioneer_orientation);
        }
    }
      
    void resetPIDVars(){
        count_pid = 0;
        //old_error = 0;
        i_error = 0;
        diff_error = 0;
        error = 0;
    }

    @Override
    public void calculateActivation() {
        
    }

    @Override
    public void proc() {
        try {
            Thread.sleep(90);
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        //i = 1.0;
       /* System.out.println("**********************************Inicio Turn Away**********************************");
        if (!winnersList.isEmpty()){
            PIDController("Turn away from Winner");
        }
        
        System.out.println("**********************************Fim Turn Away**********************************");
        */
               
        System.out.println("**********************************Inicio Turn Towards**********************************");
        if (!winnersList.isEmpty()){
            Kd_angle = i;
            PIDController("Turn towards to Winner");
            oc.pioneer_position.resetData();//reinicia com robô em outra posição
            i = i + 1;
            time_graph = 1;
        }
        if(i > 12)
            System.exit(0);
        
        System.out.println("**********************************Fim Turn Towards**********************************");
        
        
    }
    
    private void printToFile(Object ob){
        //if(time_graph < 150){
        try(FileWriter fw = new FileWriter("PIDOrien_Kp" + (int)Kp_angle + "_Ki"+ (double)Ki_angle + "_Kd"+(double)Kd_angle +".txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(time_graph+" "+ ob);
            time_graph++;
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //}
        
    }
}
