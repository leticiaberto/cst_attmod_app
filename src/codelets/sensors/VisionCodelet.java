/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.sensors;

/**
 *
 * @author leticia
 */
import CommunicationInterface.SensorI;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import codelets.motor.Lock;
import java.util.List;

/**
 * Vision codelet is responsible for getting vision information 
 * from the environment. It returns all objects in the visual field
 *  an its properties.
 *  
 * 
 */
public class VisionCodelet extends Codelet{
    
	private MemoryObject visionMO;
        private SensorI vision;


	public VisionCodelet(SensorI cam) {
            vision = cam;		

	}

	@Override
	public void accessMemoryObjects() {
		visionMO=(MemoryObject)this.getOutput("VISION");
	}

	@Override
	public void proc() {
             visionMO.setI(vision.getData());
	}//end proc()

	@Override
	public void calculateActivation() {

	}



}// class end

