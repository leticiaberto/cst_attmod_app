/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package codelets.sensors;

import br.unicamp.cst.core.entities.MemoryObject;
import br.unicamp.cst.sensorial.FeatMapCodelet;
import codelets.motor.Lock;
import cst_attmod_app.AgentMind;
import static cst_attmod_app.AgentMind.buffersize;
import static cst_attmod_app.AgentMind.laserdimension;
import static cst_attmod_app.AgentMind.sonardimension;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import outsideCommunication.SonarData;
import org.opencv.core.Core;//Pra usar OpenCV
import org.opencv.core.Mat;//Pra usar OpenCV
import org.opencv.core.*;
import org.opencv.highgui.*;
import outsideCommunication.Client;
import outsideCommunication.OutsideCommunication;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author leticia
 */
public class ImageFeatMapCodelet extends FeatMapCodelet {
    private  int time_graph;
    private Client client;
    
    public ImageFeatMapCodelet(int nsensors, ArrayList<String> sens_names, String featmapname,int timeWin, int mapDim) {
        super(nsensors, sens_names, featmapname,timeWin,mapDim);
        this.time_graph = 0;
        
        //Conect to Server
        try {

            InetAddress host = InetAddress.getByName("127.0.0.1");
            client = new Client(host, 65532);
            System.out.println("Connected to Server " );

           //client.clean();
         }catch (IOException e) {
             System.out.println("Connection between Server and Client Caught Exception: " + e.toString());
            Logger.getLogger(ImageFeatMapCodelet.class.getName()).log(Level.SEVERE, null, e);
         }   
    }

    @Override
    public void calculateActivation() {
        
    }

    @Override
    public void proc() {
        /*try {
            client.send("Hello\n");
             String response = client.recv();
            System.out.println("Client received: " + response);
*/
            //Sensor_buffers contem os nomes dos MOs que são input do Codelet. No ImageFeatMapCodelet usamos apenas o vision_bufferMO
            MemoryObject vision_bufferMO = (MemoryObject) sensor_buffers.get(0);//Retorna o MO VISION_BUFFER 
            
            List ImageData_buffer;
            
            ImageData_buffer = (List) vision_bufferMO.getI();//Pega oq tem no MO VISION_BUFFER (que é composto por um list de Integer)
            
            System.out.println("Image buff MO "+vision_bufferMO);
            
            
            
            //Pega o Memory Object (MO) associado a esse FeatureMap
            List imageFM = (List) featureMap.getI();
            
            //Tira o elemento mais antigo
            if(imageFM.size() == timeWindow){
                imageFM.remove(0);
            }
            
            //Adiciona um objeto ArrayList no List de FeatureMap
            imageFM.add(new ArrayList<>());
            //System.out.println("IImageFM size "+imageFM.size());
            int t = imageFM.size()-1;
            
            ArrayList<Float> imageFM_t = (ArrayList<Float>) imageFM.get(t);
            
            for (int j = 0; j < mapDimension; j++) {
                imageFM_t.add(new Float(0));
            }
            
            MemoryObject ImageDataMO;
            //System.out.println("Image buffer size " + ImageData_buffer.size());
            if(ImageData_buffer.size() < 1){
                return;
            }
            
            //Pega o ultimo MO (Mais recente) que compoem o list de Vision_Buffer
            ImageDataMO = (MemoryObject)ImageData_buffer.get(ImageData_buffer.size()-1);
            
            List ImageData;
            
            //Pega os valores que estao nesse MO
            ImageData = (List) ImageDataMO.getI();
            
            //enviar imagem para o server
            String aux = "12345";
                if(!ImageData.isEmpty()){
                    try {
                        System.out.println("Try to Send to Server " + aux.toString().getBytes().length);
                        System.out.println("Try to Send to  " + aux.toString().length());
                        client.send("hello\n");
                        String response = client.recv();
                        System.out.println("Client received: " + response);
                        
                    } catch (IOException ex) {
                        Logger.getLogger(ImageFeatMapCodelet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                
                //String aux = ImageData.toString();
                //System.out.println("Image aux "+aux);
                //client.send(ImageData.toString());

               // String response = client.recv();
                //System.out.println("Client received: " + response);

                //client.clean();
            
            //}catch (IOException e) {
            //System.out.println("Server/Client Caught Exception: " + e.toString());
            //}
            
            //Colocar o valor retornado da Salicon. Por enquanto esta esses valores para rodar
            for (int j = 0; j < (256*256*3); j++) {
                imageFM_t.set(j, new Float(0));
            }
            
            printToFile(imageFM_t);
            
        
    }
    
    private void printToFile(ArrayList<Float> arr){
        if(time_graph < 50){
            try(FileWriter fw = new FileWriter("imageFM.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                out.println(time_graph+" "+ arr);
                time_graph++;
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }    
}

