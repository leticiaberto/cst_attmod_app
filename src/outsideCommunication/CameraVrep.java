/*
 * /*******************************************************************************
 *  * Copyright (c) 2012  DCA-FEEC-UNICAMP
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the GNU Lesser Public License v3
 *  * which accompanies this distribution, and is available at
 *  * http://www.gnu.org/licenses/lgpl.html
 *  * 
 *  * Contributors:
 *  *     K. Raizer, A. L. O. Paraense, R. R. Gudwin - initial API and implementation
 *  ******************************************************************************/
 
package outsideCommunication;

import CommunicationInterface.SensorI;
import JEP.JavaPythonCommunication;
import coppelia.CharWA;
import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.IntWA;
import coppelia.remoteApi;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import jep.Jep;
import jep.JepConfig;
import jep.JepException;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import static org.opencv.core.CvType.CV_32FC1;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.core.Mat;
import outsideCommunication.Client;

/**
 *
 * @author leandro
 */
public class CameraVrep implements SensorI {
    private  int time_graph;
    private int res = 256;
    private remoteApi vrep;
    private IntW vision_handle;
    private int clientID;
    private List<Integer> camera_data;
    private Client client;
    
    public CameraVrep(int clientid, IntW vision_handle, remoteApi rapi){
        this.time_graph = 0;
        camera_data = Collections.synchronizedList(new ArrayList<>());
        vrep = rapi;
        this.vision_handle = vision_handle;
        clientID = clientid;
        
        for (int i = 0; i < res*res*3; i++) {//o limite foi tirado do tamanho da imagem retornada no vision_sensor (resolução*resolução*3(rgb))
            camera_data.add(0);
        }
    }
    
    @Override
    public Object getData() {
        try {
            Thread.sleep(50);
//            // System.out.println("\u001B[31m"+"TRY CATCH");
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
        
        CharWA image = new CharWA(res*res*3);
        IntWA resolution = new IntWA(2);
        int errorCode;
        
        vrep.simxGetVisionSensorImage(clientID, vision_handle.getValue(), resolution, image, 0, vrep.simx_opmode_streaming); 
        
        errorCode = vrep.simxGetVisionSensorImage(clientID, vision_handle.getValue(), resolution, image, 0, vrep.simx_opmode_buffer);
        if (errorCode == remoteApi.simx_return_ok){
            //System.out.println("Resolution: " + Arrays.toString(resolution.getArray()));               
            //System.out.format("%d %d %d %d %d\n",(int)image.getArray()[0],(int)image.getArray()[1],(int)image.getArray()[2],(int)image.getArray()[3], (int)image.getArray()[4]);
        }
        
        //Armazena os valores vindo da camera do vrep
        for (int i = 0; i < image.getArray().length; i++){
            //System.out.format("%d ",(int)image.getArray()[i]);
            camera_data.set(i, (int)image.getArray()[i]);
        }
        
        //Rodar openCV
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    

     /*percorri o array vindo do vrep (RGB) e coloquei em um MAT(BGR). A dimensão
     é a mesma vindo do vrep(256,256), mas cada posição tem 3 valores(canais)*/
        //https://answers.opencv.org/question/120398/how-to-create-mat-from-rgb-2d-arrays/   
        Mat om = new Mat(res, res, CvType.CV_8UC3);//Mat criada com os valores vindo do Vrep

        //Valores RGB vindo do Vrep. Cada valor é adicionado em um vetor para poder inserir no Mat
        double[] rx = new double[3];
        int r = 0;//indice correspondente a cor no vrep
        int g = 1;
        int b = 2;
        for(int i=0;i< om.height();i++){
            for(int j=0;j< om.width();j++){
                rx[0]= camera_data.get(b);
                rx[1]=camera_data.get(g);
                rx[2]=camera_data.get(r);
                om.put(i, j, rx);
                b = b + 3;//sempre sera RGB, logo para repetir anda a posição atual + 3
                g = g + 3;
                r = r + 3;
            }
        }
        //Apenas para testar se Mat ta correto
        //if(time_graph == 5){
            //Core.flip(om, om, 0);//flip na vertical (pra ficar correto)
            //HighGui.namedWindow("Rec", HighGui.WINDOW_AUTOSIZE);
           // HighGui.imshow("Rec", om);
            //Imgcodecs.imwrite("ImageMat.png", om);
            //HighGui.waitKey();
            //printToFileMat(om.dump());//deixar comentado
            //callPythonSalicon(om);
        //}

        //System.out.println("SIZEOM"+ om.size().toString());
         //printToFileMat(om.dump());Esse tem que deixar

       
        
        System.out.println();
        System.out.println("TimeGraph" + time_graph);
        // SYNC
        if (vrep.simxSynchronous(clientID, true) == remoteApi.simx_return_ok)
  			vrep.simxSynchronousTrigger(clientID);
        //printToFile(camera_data);//salva todos os valores vindo da camera no vrep + instante de tempo
        
        //Abrir imagem que esta no diretorio e mostrar na tela" - Somente para teste
        //Mat img = Imgcodecs.imread("imgReconstruida.png");
        //HighGui.namedWindow("image", HighGui.WINDOW_AUTOSIZE);
        //HighGui.imshow("image", img);
       // HighGui.waitKey();
       //System.out.println("SIZE"+ img.size().toString());        
        //callPythonSalicon(om);
        //return camera_data;//verificar se retorna essa variavel ou Mat om
       // return om.dump();
       //if(time_graph == 5){
       /*try {
            System.out.println("Try to Send to Server " );
            InetAddress host = InetAddress.getByName("127.0.0.1");
            client = new Client(host, 65532);

            client.send(om.dump());
           
           //String response = client.recv();
           //System.out.println("Client received: " + response);
            
           client.clean();
            
         }catch (IOException e) {
            System.out.println("Server/Client Caught Exception: " + e.toString());
         }*/
       
       //return om;
       //time_graph++;
        System.out.println("Size camera: " + camera_data.size());
       printToFile(camera_data);
       return camera_data;
    }
    
    private void printToFileMat(Object object){
        if(time_graph < 50){
            try(FileWriter fw = new FileWriter("MatDumpValues.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                out.println(time_graph+" "+ object);
                //time_graph++;//ja é dado no printToFile
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void printToFile(Object object){
        if(time_graph < 50){
            try(FileWriter fw = new FileWriter("camera.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                out.println(time_graph+" "+ object);
                time_graph++;
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private void callPythonSalicon(Mat omValues){
        
        System.out.println("Exemplo 01");
        
        JavaPythonCommunication data = new JavaPythonCommunication();
        //To be able to call Python we need to have a Jep object:
        Jep jep;
        try {
            JepConfig cfg = new JepConfig().addIncludePaths("/home/leticia/caffe/python/").setInteractive(true);
            jep = new Jep(cfg);
            //set a single variable so we can access it from Python
            //jep.eval("import sys");
            //jep.eval("sys.path.insert(0, '/home/leticia/caffe/python/')");
            jep.set("javaMatValues", omValues.dump());
            //set our object that we'll put our data back into
            //jep.set("saliencyFromPython", data);
            //Now we run the script (this should be located in the current directory, //or you can use the full path to the script)
            //jep.runScript("OpenSALICON/teste.py");
            //Now print the data we got from the Script into our object
            //System.out.println("PYTHON SAID:" + data.getTestString());
            //System.out.println("PYTHON PATH SAID:" + pathpy.getTestString());
            jep.close();
        } catch (JepException ex) {
            //System.out.println("Erro 01");
            Logger.getLogger(CameraVrep.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

	@Override
	public void resetData() {
		// TODO Auto-generated method stub
		
	}
        
    
    
}
