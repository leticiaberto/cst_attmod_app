/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author leticia
 */
package outsideCommunication;
import java.io.*;
import java.net.*;


public class Client
{
   private Socket socket = null;
   
   private BufferedReader reader = null;
   private BufferedWriter writer = null;
   
   
   
   public Client(InetAddress address, int port) throws IOException
   {
      socket = new Socket(address, port);
      reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
      
   }

   public void send(String msg) throws IOException
   {
      writer.write(msg, 0, msg.length());
      writer.flush();
   }
   
   
   public String recv() throws IOException
   {
      return reader.readLine();
   }
   
   public void clean() throws IOException{
       
        socket.shutdownInput();
        socket.shutdownOutput();
        socket.close();

        writer.flush();
        writer.close();

        reader.close();   
   }
}





/*public class Client
{
   static private List<Integer> camera_data;
   private Socket socket = null;
   private ObjectOutputStream objectOutput = null;
   private ObjectInputStream objectInput = null;
   
   public Client(InetAddress address, int port) throws IOException
   {
      socket = new Socket(address, port);      
      objectOutput = new ObjectOutputStream(socket.getOutputStream());
      //objectInput =  new ObjectInputStream(socket.getInputStream()); 
   }
 
    public void sendArray(List<Float> camera_data) throws IOException {
        objectOutput.writeObject(camera_data);
    }

   public List<Integer> recvArray() throws IOException, ClassNotFoundException
   {
      return (List<Integer>) objectInput.readObject();
   }
    
}*/

