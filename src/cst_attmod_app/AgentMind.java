/*
 * /*******************************************************************************
 *  * Copyright (c) 2012  DCA-FEEC-UNICAMP
 *  * All rights reserved. This program and the accompanying materials
 *  * are made available under the terms of the GNU Lesser Public License v3
 *  * which accompanies this distribution, and is available at
 *  * http://www.gnu.org/licenses/lgpl.html
 *  * 
 *  * Contributors:
 *  *     K. Raizer, A. L. O. Paraense, R. R. Gudwin - initial API and implementation
 *  ******************************************************************************/
 
package cst_attmod_app;

//import br.unicamp.cst.attention.DecisionMakingCodelet;
//import br.unicamp.cst.attention.SaliencyMapCodelet;
//import br.unicamp.cst.attention.Winner;
import codelets.sensors.ImageFeatMapCodelet;
import attention.DecisionMakingCodelet;
import attention.SaliencyMapCodelet;
import attention.Winner;
import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.Memory;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import br.unicamp.cst.core.entities.Mind;
import br.unicamp.cst.sensorial.SensorBufferCodelet;
import codelets.behaviors.DoNothing;
import codelets.behaviors.MoveForward;
import codelets.behaviors.PIDController;
import codelets.behaviors.Turn;
import codelets.motor.MotorCodelet;
import codelets.sensors.AppCombFeatMapCodelet;
import codelets.sensors.DirectionFeatMapCodelet;
import codelets.sensors.DistanceFeatMapCodelet;
import codelets.sensors.LaserCodelet;
import codelets.sensors.SonarCodelet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import outsideCommunication.OutsideCommunication;
import outsideCommunication.SonarData;
import codelets.learner.LearnerCodelet;
import codelets.learner.NewLearnerCodelet;
import codelets.motor.MotorController;
import codelets.sensors.GroundTruthCodelet;
import codelets.sensors.VisionCodelet;
import coppelia.FloatWA;


/**
 *
 * @author leandro
 */
public class AgentMind extends Mind {
    
    public static final int buffersize = 50;
    public static final int laserdimension = 182;
    public static final int sonardimension = 8;
    public static final int res = 256;//Leticia
    public static final int imagedimension = res*res*3;//Leticia
    
    public AgentMind(OutsideCommunication oc, String mode){
        
        super();
        
        //////////////////////////////////////////////
        //Declare Memory Objects
        //////////////////////////////////////////////
        
        //Motors
        /*MemoryObject motorActionMO;
        MemoryObject left_motor_speed;
        MemoryObject right_motor_speed;
        
        motorActionMO = createMemoryObject("MOTOR", "");
        left_motor_speed = createMemoryObject("L_M_SPEED", new Float(0));
        right_motor_speed  = createMemoryObject("R_M_SPEED", new Float(0));*/
        
        //Container para Motor - Pois possuem mais de um comportamento
        MemoryContainer behaviorMO;//trocar nome depois
        behaviorMO = createMemoryContainer("BEHAVIOR");
        
        /*ArrayList<Memory> motorMOs = new ArrayList<>();
        motorMOs.add(motorActionMO);
        motorMOs.add(left_motor_speed);
        motorMOs.add(right_motor_speed);*/
        
        
        //Sensors
        //Sonars
        SonarData sonarData = new SonarData();
        MemoryObject sonar_read = createMemoryObject("SONARS", sonarData);
        
        //Laser
        List laser_data = Collections.synchronizedList(new ArrayList<Float>(laserdimension));
        MemoryObject laser_read = createMemoryObject("LASER",laser_data);
        
        
        //Camera - Leticia
        List vision_list = Collections.synchronizedList(new ArrayList<Integer>(imagedimension));
	MemoryObject visionMO = createMemoryObject("VISION",vision_list);
        
        List groundT = Collections.synchronizedList(new ArrayList<ArrayList<FloatWA>>());
        MemoryObject groundTMO = createMemoryObject("GTMO",groundT); 
        
                
        //Sensor Buffers
        //Sonars
        List sonar_buffer_list = Collections.synchronizedList(new ArrayList<Memory>(buffersize));
        MemoryObject sonar_bufferMO = createMemoryObject("SONAR_BUFFER",sonar_buffer_list);
        
        //Laser
        List laser_buffer_list = Collections.synchronizedList(new ArrayList<Memory>(buffersize));
        MemoryObject laser_bufferMO = createMemoryObject("LASER_BUFFER",laser_buffer_list);
        
        //Camera - Colocar de volta qnd estiver usando Visao
        /*
        List vision_buffer_list = Collections.synchronizedList(new ArrayList<Memory>(buffersize));
        MemoryObject vision_bufferMO = createMemoryObject("VISION_BUFFER",vision_buffer_list);*/
        
        // Feature Maps
        
        //Direction
        List directionFM = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());      
        MemoryObject direction_fmMO = createMemoryObject("DIRECTION_FM", directionFM);
        
        //Distance
        List distFM = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());  
        MemoryObject dist_fmMO = createMemoryObject("DISTANCE_FM", distFM);
        
        
        //Saliency Image - Leticia - Colocar de volta qnd estiver usando Visao
        /*
        List visionFM = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());//ver se é integer ou float o correto
        MemoryObject vision_fmMO = createMemoryObject("IMAGE_FM", visionFM);*/
        
        //
        //Combined Feat Map
        //
        
        List weights = Collections.synchronizedList(new ArrayList<Float>(2));
        weights.add(1.0f);
        weights.add(1.0f);
        MemoryObject weightsMO = createMemoryObject("FM_WEIGHTS",weights);
        
        List CombFM = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());    
        MemoryObject combFMMO = createMemoryObject("COMB_FM",CombFM);
        
        //ATTENTIONAL MAP
        
        List att_mapList = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());
        MemoryObject attMapMO = createMemoryObject("ATTENTIONAL_MAP",att_mapList);
        
        //WINNERS
        
        List winnersList = Collections.synchronizedList(new ArrayList<Winner>());
        MemoryObject winnersMO = createMemoryObject("WINNERS",winnersList);
        
        //SALIENCY MAP
        
        List saliencyMap = Collections.synchronizedList(new ArrayList<ArrayList<Float>>());
        MemoryObject salMapMO = createMemoryObject("SALIENCY_MAP", saliencyMap);
        
        //ACTIONS 
        
        //List actionsList = Collections.synchronizedList(new ArrayList<String>());
        //MemoryObject actionsMO = createMemoryObject("ACTIONS", actionsList);
        
        List actionsList = Collections.synchronizedList(new ArrayList<ArrayList>());
        MemoryObject actionsMO = createMemoryObject("ACTIONS",actionsList); 
        
        //STATES 
        
        List statesList = Collections.synchronizedList(new ArrayList<String>());
        MemoryObject statesMO = createMemoryObject("STATES", statesList);
//        
//        
//        ////////////////////////////////////////////
//        //Codelets
//        ////////////////////////////////////////////
////        
        //Motors - Ate versão da Carol
        /*Codelet motors = new MotorCodelet(oc.right_motor, oc.left_motor);
        motors.addInputs(motorMOs);
        insertCodelet(motors);*/
        
        /*Codelet motors = new MotorController(oc.right_motor, oc.left_motor);
        motors.addInputs(motorMOs);
        insertCodelet(motors);
        
        Codelet pid = new PIDController(oc);
        pid.addInput(winnersMO);
        pid.addInput(groundTMO);
        pid.addOutputs(motorMOs);
        insertCodelet(pid);*/
                
        
        //Sensors
        Codelet sonars = new SonarCodelet(oc.sonar);
        sonars.addOutput(sonar_read);
        insertCodelet(sonars);
        
        Codelet laser = new LaserCodelet(oc.laser);
        laser.addOutput(laser_read);
        insertCodelet(laser);
        
        //Leticia - Colocar de volta qnd estiver usando Visao
        /*Codelet vision = new VisionCodelet(oc.camera);
        vision.addOutput(visionMO);
        insertCodelet(vision); //Creates a vision sensor*/
        
        Codelet groundTruth = new GroundTruthCodelet(oc.pioneer_orientation, oc.pioneer_position, oc.sonar_orientations);
        groundTruth.addOutput(groundTMO);
        insertCodelet(groundTruth);
        
        
        //Sensor Buffers
        //Sonar
        Codelet sonar_buffer = new SensorBufferCodelet("SONARS", "SONAR_BUFFER", buffersize);
        sonar_buffer.addInput(sonar_read);
        sonar_buffer.addOutput(sonar_bufferMO);
        insertCodelet(sonar_buffer);
        
        //Laser
        Codelet laser_buffer = new SensorBufferCodelet("LASER", "LASER_BUFFER", buffersize);
        laser_buffer.addInput(laser_read);
        laser_buffer.addOutput(laser_bufferMO);
        insertCodelet(laser_buffer);
        
        //Camera - Leticia - Colocar de volta qnd estiver usando Visao
        /*
        Codelet vision_buffer = new SensorBufferCodelet("VISION", "VISION_BUFFER", buffersize);
        vision_buffer.addInput(visionMO);
        vision_buffer.addOutput(vision_bufferMO);
        insertCodelet(vision_buffer);*/
        
        //Feat Maps
        
        //Direction
        ArrayList<String> sensbuff_names = new ArrayList<>();
        sensbuff_names.add("SONAR_BUFFER");
        sensbuff_names.add("LASER_BUFFER");
        
        // System.out.println("size sens buff name "+sensbuff_name.size());
        
        Codelet direction_fm_c = new DirectionFeatMapCodelet(sensbuff_names.size(), sensbuff_names, "DIRECTION_FM",buffersize,sonardimension);
        direction_fm_c.addInput(sonar_bufferMO);
        direction_fm_c.addInput(laser_bufferMO);
        direction_fm_c.addOutput(direction_fmMO);
        insertCodelet(direction_fm_c);

        //Distance
        ArrayList<String> sensbuff_names_dist = new ArrayList<>();
        sensbuff_names_dist.add("LASER_BUFFER");
        
        // System.out.println("size sens buff name "+sensbuff_names.size());
        
        Codelet dist_fm_c = new DistanceFeatMapCodelet(sensbuff_names_dist.size(), sensbuff_names_dist, "DISTANCE_FM",buffersize,laserdimension);
        dist_fm_c.addInput(laser_bufferMO);
        dist_fm_c.addOutput(dist_fmMO);
        insertCodelet(dist_fm_c);
        
        //Image - Leticia - Colocar de volta qnd estiver usando Visao
        /*
        ArrayList<String> sensbuff_names_image = new ArrayList<>();
        sensbuff_names_image.add("VISION_BUFFER");*/
        
        //Colocar de volta qnd estiver usando Visao
        /*
        Codelet image_fm_c = new ImageFeatMapCodelet(sensbuff_names_image.size(), sensbuff_names_image, "IMAGE_FM",buffersize,imagedimension);
        image_fm_c.addInput(vision_bufferMO);
        image_fm_c.addOutput(vision_fmMO);
        insertCodelet(image_fm_c);*/
                
        ArrayList<String> FMnames = new ArrayList<>();
        FMnames.add("DIRECTION_FM");
        FMnames.add("DISTANCE_FM");        
        //FMnames.add("IMAGE_FM"); 
        
        Codelet comb_fm_c = new AppCombFeatMapCodelet(FMnames.size(), FMnames,buffersize,sonardimension);
        comb_fm_c.addInput(direction_fmMO);
        comb_fm_c.addInput(dist_fmMO);
        comb_fm_c.addInput(weightsMO);
        //comb_fm_c.addInput(vision_fmMO);
        comb_fm_c.addOutput(combFMMO);
        insertCodelet(comb_fm_c);
        
        //SALIENCY MAP CODELET
        Codelet sal_map_cod = new SaliencyMapCodelet("SALIENCY_MAP", "COMB_FM", "ATTENTIONAL_MAP", buffersize, sonardimension);
        sal_map_cod.addInput(combFMMO);
        sal_map_cod.addInput(attMapMO);
        sal_map_cod.addOutput(salMapMO);
        insertCodelet(sal_map_cod);
        
        //DECISION MAKING CODELET
        Codelet dec_mak_cod = new DecisionMakingCodelet("WINNERS", "ATTENTIONAL_MAP", "SALIENCY_MAP", buffersize, sonardimension);
        dec_mak_cod.addInput(salMapMO);
        dec_mak_cod.addOutput(winnersMO);
        dec_mak_cod.addOutput(attMapMO);
        insertCodelet(dec_mak_cod);
        
        /*Codelet learner_cod = new LearnerCodelet(oc, buffersize, sonardimension, mode);
        learner_cod.addInput(salMapMO);
        learner_cod.addInput(winnersMO);
        learner_cod.addInput(sonar_read);
        learner_cod.addInput(groundTMO);
        learner_cod.addOutputs(motorMOs);
        learner_cod.addOutput(actionsMO);
        learner_cod.addOutput(statesMO);
        insertCodelet(learner_cod);*/
        
        Codelet newLearner_cod = new NewLearnerCodelet(oc, buffersize, sonardimension, mode);
        newLearner_cod.addInput(salMapMO);
        newLearner_cod.addInput(winnersMO);
        //newLearner_cod.addInput(sonar_read);
        newLearner_cod.addInput(sonar_bufferMO);
        newLearner_cod.addInput(groundTMO);
        newLearner_cod.addOutput(actionsMO);
        newLearner_cod.addOutput(statesMO);
        newLearner_cod.addOutput(behaviorMO);
        insertCodelet(newLearner_cod);
        
        //Behaviors
        Codelet moveF = new MoveForward();
        moveF.addInput(actionsMO);
        moveF.addOutput(behaviorMO);
        insertCodelet(moveF);
        
        Codelet turn = new Turn();
        turn.addInput(actionsMO);
        turn.addInput(winnersMO);
        turn.addInput(groundTMO);
        turn.addOutput(behaviorMO);
        insertCodelet(turn);
        
        Codelet doNot = new DoNothing();
        doNot.addInput(actionsMO);
        doNot.addOutput(behaviorMO);
        insertCodelet(doNot);
        
        //MOTOR versão Leticia
        //Novo MOTOR codelet utilizando container para decidir ação
        Codelet motors = new MotorCodelet(oc.right_motor, oc.left_motor);
        motors.addInput(behaviorMO);
        insertCodelet(motors);

        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        
        // NOTE Sets the time interval between the readings
        // sets a time step for running the codelets to avoid heating too much your machine
        for (Codelet c : this.getCodeRack().getAllCodelets())
            c.setTimeStep(200);
	
        try {
            Thread.sleep(200);
        } catch (Exception e) {
            Thread.currentThread().interrupt();
        }
        
     
	// Start Cognitive Cycle
	start(); 
	
    }
}
