/*
 * Copyright (C) 2019 leticia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package JEP;

/**
 *
 * @author leticia
 */
public class JavaPythonCommunication {
    String test;
    Integer numero;
   public void setTestString(String s)
   {
      test = s;
   }
   public String getTestString()
   {
      return test;
   }
   
   public void setTestInt(Integer num)
   {
      numero = num;
   }

    Integer getTestNumero() {
        return numero;
    }
}
